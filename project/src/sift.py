# -*- coding: utf-8 -*-
"""
Created on Mon Nov 20 15:59:50 2017

@author: thens
"""

from Img import Img
from matplotlib import pyplot as plt
import cv2
from Ransac import Ransac 
from MatchPoints import MatchPoints

img = Img.imagefile("iit.jpg")
img1 = Img.imagefile("iit_rotate_small.jpg")
#cv2.imwrite("iit_rotate_small.jpg",cv2.resize(img1.image_data,(img1.image_data.shape[0]*8//10,img1.image_data.shape[1]*8//10)))


img.getImageFeatures()
fc,fimg = img.writeImageFeatures()

img1.getImageFeatures()
fc1,fimg1 = img.writeImageFeatures()

p1,p2,c=img.matchImage(img1,ratio=0.9)
m = img.drawMatch(img1,p1,p2)

curr_figsize = plt.rcParams["figure.figsize"]
plt.rcParams["figure.figsize"] = (15,10)

ax1 = plt.subplot2grid((1,3),(0,0))
ax2 = plt.subplot2grid((1,3),(0,1))
ax3 = plt.subplot2grid((1,3),(0,2))

ax1.imshow(cv2.cvtColor(m,cv2.COLOR_BGR2RGB))
ax1.set_xticks([])
ax1.set_yticks([])
ax1.set_xlabel(str(c)+ " Matches, ratio = (0.9)")

p1,p2,c=img.matchImage(img1,ratio=0.8)
m = img.drawMatch(img1,p1,p2)


ax2.imshow(cv2.cvtColor(m,cv2.COLOR_BGR2RGB))
ax2.set_xticks([])
ax2.set_yticks([])
ax2.set_xlabel(str(c)+ " Matches, ratio = (0.8)")


p1,p2,c=img.matchImage(img1,ratio=0.6)
m = img.drawMatch(img1,p1,p2)


ax3.imshow(cv2.cvtColor(m,cv2.COLOR_BGR2RGB))
ax3.set_xticks([])
ax3.set_yticks([])
ax3.set_xlabel(str(c)+ " Matches, ratio = (0.6)")

plt.tight_layout()
plt.show(block=False)
plt.rcParams["figure.figsize"] = curr_figsize


#curr_figsize = plt.rcParams["figure.figsize"]
#plt.rcParams["figure.figsize"] = (15,10)
#
#
#ax1 = plt.subplot2grid((1,2),(0,0))
#ax2 = plt.subplot2grid((1,2),(0,1))
#ax1.imshow(cv2.cvtColor(img.image_data,cv2.COLOR_BGR2RGB))
#ax1.set_xlabel("Image",fontsize=16)
#ax1.set_xticks([])
#ax1.set_yticks([])
#ax2.imshow(cv2.cvtColor(fimg,cv2.COLOR_BGR2RGB))
#ax2.set_xlabel(str(fc)+ " Features",fontsize=16)
#ax2.set_xticks([])
#ax2.set_yticks([])
#plt.tight_layout()
#plt.show(block=False)
#
#plt.rcParams["figure.figsize"] = curr_figsize
