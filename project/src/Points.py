import cv2
import numpy as np
import sys
import os
from operator import itemgetter
from matplotlib import pyplot as plt

# This class is required for RANSAC. RANSAC operates on this dataset.
# contains the set of points to be compared and the fit,score functions 
class MatchPoints:
    def __init__(self,p1,p2):
        # check if p1 and p2 is same length
        if len(p1) != len(p2):
            print ("ERROR: p1 and p2 not of same length")
            return
        self.p1 = p1
        self.p2 = p2
        self.N  = len(self.p1)
        
    def drawSamples(self):
        # get 4 random indices
        r = np.random.randint(0,self.N,4)
        #print ("INFO: Random", r)
        rp1 = np.zeros((4,2),self.p1.dtype)
        rp2 = np.zeros((4,2),self.p2.dtype)
        
        rp1[0,:] = self.p1[r[0],:]
        rp1[1,:] = self.p1[r[1],:]
        rp1[2,:] = self.p1[r[2],:]
        rp1[3,:] = self.p1[r[3],:]

        rp2[0,:] = self.p2[r[0],:]
        rp2[1,:] = self.p2[r[1],:]
        rp2[2,:] = self.p2[r[2],:]
        rp2[3,:] = self.p2[r[3],:]
        
        return rp1,rp2 

    # find a homography matrix for the 4 points that is passed using opencv
    def findHomography4CV(self,rp1, rp2):
        homography, status = cv2.findHomography(rp1, rp2, 0, 0)
        return homography,status

    # find a homography matrix for the 4 points that is passed using own SVD
    def findHomography4OwnSVD(self,rp1, rp2):
        homography, status = GetHomographyMatrixUsingSVD(rp1,rp2)
        return homography,status
    
    def computeInliers(self,H,t=3):
        # convert to homogenous and back
        # https://stackoverflow.com/questions/8486294/how-to-add-an-extra-column-to-an-numpy-array
        p1_h = np.zeros((self.p1.shape[0], self.p1.shape[1]+1),self.p1.dtype)
        p1_h[:,:-1]= self.p1
        p1_h[:,2] = 1.0
        # https://stackoverflow.com/questions/26289972/use-numpy-to-multiply-a-matrix-across-an-array-of-points
        dest = p1_h.dot(H.T)
        # convert back to non-homogenous co-ordinates
        dest1 = np.zeros(self.p1.shape,self.p1.dtype)
        dest1[:,0] = dest[:,0] / dest[:,2]
        dest1[:,1] = dest[:,1] / dest[:,2]
        
        # Compute residuals, inmask and score 
        #   remember x and y is flipped in the image array
        #   point is inlier if both X and Y are within the the threshold
        residuals_y = dest1[:,0] - self.p2[:,0]
        residuals_x = dest1[:,1] - self.p2[:,1]
        score       = np.sum(residuals_x**2 + residuals_y**2)
        inmask_x    = np.abs(residuals_x) < t
        inmask_y    = np.abs(residuals_y) < t
        inmask      = np.logical_and(inmask_x,inmask_y)
        
        # count inliners, w
        n_in  = np.sum(inmask)
        n_out = len(self.p1) - n_in
        w = float(n_in)/(n_in+n_out)
        return n_in, n_out, w, score, inmask
    
    # Lets summarize the H matrix computed from all methods
    # 1) opencv homography for all points without RANSAC
    # 2) opencv homography for all points with opencv RANSAC
    # 3) opencv homography4 computed by our own RANSAC
    # 4) opencv homography for all inlier points determined by our own RANSAC
    def summarizeMethods(self,ransac1_H,ransac1_inmask,ransac2_H,ransac2_inmask,t=3):
        
        hSummary = {}
        H_cv_R_none,s1    = cv2.findHomography(self.p1, self.p2,0)
        H_cv_R_cv,s2      = cv2.findHomography(self.p1, self.p2,cv2.RANSAC,t)
        H_cv4_R_own       = ransac1_H
        H_own_R_own       = ransac2_H
        H_cvmask_R_own,s4 = cv2.findHomography(self.p1[ransac1_inmask], self.p2[ransac1_inmask],0)

        hSummary['H_cv_R_none']    = {}
        hSummary['H_cv_R_cv']      = {}
        hSummary['H_cv4_R_own']    = {}
        hSummary['H_own_R_own']    = {}
        hSummary['H_cvmask_R_own'] = {}
        
        hSummary['H_cv_R_none']['H']    = H_cv_R_none
        hSummary['H_cv_R_cv']['H']      = H_cv_R_cv
        hSummary['H_cv4_R_own']['H']    = H_cv4_R_own
        hSummary['H_own_R_own']['H']    = ransac2_H
        hSummary['H_cvmask_R_own']['H'] = H_cvmask_R_own
        
        for method in hSummary.keys():
            H = hSummary[method]['H']
            n_in, n_out, w, score, inmask = self.computeInliers(H,t)
            hSummary[method]['score'] = score
            hSummary[method]['n_in']  = n_in
            hSummary[method]['n_out'] = n_out
            hSummary[method]['w']     = w
        
        # print the details
        print ("\n\nSummary:\nResidual Threshold = %.2f" % t)
        print ("%-20s\t%-10s\t%-10s\t%-10s\t" % ("Method", '#inliners', 'Score', '#outliers'))
        for method in hSummary.keys():
            print ("%-20s\t%d\t\t%.2f\t%d" % 
                   (method,
                    hSummary[method]['n_in'],
                    hSummary[method]['score'],
                    hSummary[method]['n_out']))
            