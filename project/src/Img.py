# -*- coding: utf-8 -*-
"""
Created on Sun Nov 19 12:37:54 2017

@author: {aiyengar,thens}@hawk.iit.edu
"""
import cv2
import numpy as np
import os
from operator import itemgetter
from matplotlib import pyplot as plt

# This class provides abstraction around an image object, will contain
#   image -> numpy image array
#   dirname
#   filename
#   extension
class Img:
    # class variables for configurable globals
    # text annotation on images
    _text_size  = 0.5
    _text_color = (255,255,255)
    _text_pos   = (20,20)
    # resize image
    _image_resize = (400,400)
    _image_match_ratio = 0.75

    def __init__(self,image_data,resize=False,directory="./", filename="image1", fileext=".png"):
        # resize the image for speed
        if resize:
            image_data = cv2.resize(image_data, self._image_resize)

        self.image_data = np.copy(image_data)
        self.directory = directory
        self.filename  = filename
        self.fileext   = fileext

    # using a class method so that we can create a Img object either from a file or 
    # from a numpy array. This is the pythonic way of doing multiple constructors
    @classmethod
    def imagefile(cls,image_file,resize=False):
        # read image and resize
        image_data = cv2.imread(image_file)
        if (image_data is None):
            print("ERROR: Failed to read image file: ", image_file)
            return image_data

        filewithoutext,fileext = os.path.splitext(image_file)
        directory = os.path.dirname(filewithoutext)
        filename  = os.path.basename(filewithoutext)

        print("Resize = ", resize)
        return cls(image_data,resize=resize,filename=filename,fileext=fileext,directory=directory)
        
    
    # Write debug or annotated images corresponding to this image
    # we borrow the outdir, filename, extension from the parent image
    # output file name = outdir/<basename>-tag.<ext>
    def writeDebugImage(self,tag,image_data=None,outdir="./debug/"):
        if outdir is None: outdir=self.directory
        if os.path.exists(outdir) is False:
            os.makedirs(outdir)
        if image_data is None: image_data = self.image_data
        outfile = outdir+"/"+self.filename+"-"+tag+self.fileext
        cv2.imwrite(outfile,image_data)
        #print ("INFO: Wrote ", outfile)


    # Returns features for the |image| using the opencv SIFT implementation.
    def getImageFeatures(self):
        sift_descriptor = cv2.xfeatures2d.SIFT_create()
        (keypoints, features) = sift_descriptor.detectAndCompute(self.image_data, None)

        self.keypoints = keypoints
        self.features  = features
        
    # annotate the keypoints on the image
    def writeImageFeatures(self):
        sift_image = np.copy(self.image_data)
        cv2.drawKeypoints(self.image_data,self.keypoints,sift_image,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        # add text on the image
        text = str(len(self.keypoints)) + " Features"
        cv2.putText(sift_image,text,self._text_pos,cv2.FONT_HERSHEY_SIMPLEX, self._text_size,self._text_color)

        self.writeDebugImage('sift',image_data=sift_image)
        return len(self.keypoints),sift_image
        
    # Matches |image1| and |image2| using the opencv SIFT descriptor and returns the matching
    # keypoints on success. On failure returns empty keypoints.
    def matchImage(self, other, ratio=None, writeMatchImage=False):
        if ratio is None:
            ratio = Img._image_match_ratio
        #Brute force matching with k nearest neighbors.  We take k = 2 to apply
        #ratio test. https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_feature2d/py_matcher/py_matcher.html
        matcher = cv2.BFMatcher()
        matches = matcher.knnMatch(self.features, other.features, k=2)
        
        # Apply ratio test
        good = []
        good_features = []
        for m, n in matches:
            if m.distance < ratio * n.distance:
                good_features.append((m.trainIdx, m.queryIdx, m.distance))
                good.append([m])
        good_features = sorted(good_features, key=itemgetter(2))

        if writeMatchImage:
            # create a numpy array for showing the matching features
            #   width  = width of image1 + width of image2
            #   height = maximum of image1, image2 (we stack along the horizontal direction)
            h1,w1,c = self.image_data.shape
            h2,w2,c = other.image_data.shape
            match_image = np.zeros((max(h1,h2),w1+w2,c),self.image_data.dtype)
            cv2.drawMatchesKnn(self.image_data,self.keypoints,other.image_data,other.keypoints,good,match_image,flags=2)
            # add text on the image
            text = str(len(good)) + " Matches"
            cv2.putText(match_image,text,self._text_pos,cv2.FONT_HERSHEY_SIMPLEX, self._text_size,self._text_color)
            self.writeDebugImage(other.filename+'-match', image_data=match_image)
        
        # we are in business if we have 4 matches
        if len(good_features) > 4:
            pts_image1 = np.float32([self.keypoints[i].pt for (_, i, _) in good_features])
            pts_image2 = np.float32([other.keypoints[i].pt for (i, _, _) in good_features])
            return (pts_image1, pts_image2, len(good_features))
        else:
            return (None, None, len(good_features))
    def drawMatch(self,other,p1,p2,square_color=(0,255,0),line_color=(255,0,0),outfile=None,text=None):
        h1,w1,c = self.image_data.shape
        h2,w2,c = other.image_data.shape
        # construct a big canvas to hold both the images and place the image side by side        
        match_image = np.zeros((max(h1,h2),w1+w2,c),self.image_data.dtype)
        h,w,c = match_image.shape
        start_h1 = (h-h1)//2
        start_h2 = (h-h2)//2
        match_image[start_h1:start_h1+h1,0:w1,:] = self.image_data
        match_image[start_h2:start_h2+h2,w1:w1+w2,:] = other.image_data
        # offset the points
        pc1 = np.copy(p1)
        pc2 = np.copy(p2)
        pc2[:,0] = pc2[:,0] + w1
        pc2[:,1] = pc2[:,1] + start_h2
        
        #plt.imshow(cv2.cvtColor(match_image,cv2.COLOR_BGR2RGB))
        #plt.scatter(p1[:,0],p1[:,1],marker="+",label="p1")
        #plt.scatter(p2[:,0],p2[:,1],marker="x",label="p2")
        #plt.legend(loc='upper right')
        #plt.show()
        for i in range(len(p1)):
            p1_x = int(pc1[i,0])
            p1_y = int(pc1[i,1])
            p2_x = int(pc2[i,0])
            p2_y = int(pc2[i,1])
            cv2.rectangle(match_image,(p1_x-1,p1_y-1),(p1_x+5,p1_y+5),square_color)
            cv2.rectangle(match_image,(p2_x-1,p2_y-1),(p2_x+5,p2_y+5),square_color)
            if line_color is not None:
                cv2.line(match_image,(p1_x,p1_y),(p2_x,p2_y),line_color,2)
        if text is not None:
            cv2.putText(match_image,text,self._text_pos,cv2.FONT_HERSHEY_SIMPLEX, self._text_size,self._text_color)
            
        #plt.imshow(cv2.cvtColor(match_image,cv2.COLOR_BGR2RGB))
        #plt.show()
        if outfile is not None:
            cv2.imwrite(outfile,match_image)
        return match_image
