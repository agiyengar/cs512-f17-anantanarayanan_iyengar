# -*- coding: utf-8 -*-
"""
Created on Sun Nov 19 12:37:54 2017

@author: {aiyengar,thens}@hawk.iit.edu
"""
import sys
import os
from PanoramaConfig import PanoramaConfig

from Panorama import Panorama

def process_commandline():
    if len(sys.argv) < 2:
        print("Insufficient arguments. The usage is image_stitcher.py <Image Directory>. Please pass the image directory\n")
        sys.exit()
    pan = Panorama()
    pan.OnNewImageDirectory(sys.argv[1], pc.get_option('homography', 'algorithm'))
    pan.RunLoop()   

# check if a config file exists 
if os.path.exists("panorama.ini"):
    pc = PanoramaConfig("panorama.ini")
    process_commandline()
else:
    print ("ERROR: Could not find the configuration file: panorama.ini")
    print ("Exiting ...")
    sys.exit()