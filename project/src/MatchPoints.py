# -*- coding: utf-8 -*-
"""
Created on Sun Nov 19 12:37:54 2017

@author: {aiyengar,thens}@hawk.iit.edu
"""
import cv2
import numpy as np

# This class is required for RANSAC. RANSAC operates on this dataset.
# contains the set of points to be compared and the fit,score functions 
class MatchPoints:
    def __init__(self,p1,p2):
        # check if p1 and p2 is same length
        if len(p1) != len(p2):
            print ("ERROR: p1 and p2 not of same length")
            return
        self.p1 = p1
        self.p2 = p2
        self.N  = len(self.p1)
        
    def drawSamples(self):
        # get 4 random indices
        r = np.random.randint(0,self.N,4)
        #print ("INFO: Random", r)
        rp1 = np.zeros((4,2),self.p1.dtype)
        rp2 = np.zeros((4,2),self.p2.dtype)
        
        rp1[0,:] = self.p1[r[0],:]
        rp1[1,:] = self.p1[r[1],:]
        rp1[2,:] = self.p1[r[2],:]
        rp1[3,:] = self.p1[r[3],:]

        rp2[0,:] = self.p2[r[0],:]
        rp2[1,:] = self.p2[r[1],:]
        rp2[2,:] = self.p2[r[2],:]
        rp2[3,:] = self.p2[r[3],:]
        
        return rp1,rp2 

    # find a homography matrix for the 4 points that is passed using opencv
    @classmethod
    def findHomography4CV(cls,rp1, rp2):
        homography, status = cv2.findHomography(rp1, rp2, 0, 0)
        return homography,status

    # find a homography matrix for the 4 points that is passed using own SVD
    @classmethod
    def findHomography4OwnSVD(cls,rp1, rp2):
        homography, status = MatchPoints.GetHomographyMatrixUsingSVD(rp1,rp2)
        return homography,status
    
    def computeInliers(self,H,t=3):
        # convert to homogenous and back
        # https://stackoverflow.com/questions/8486294/how-to-add-an-extra-column-to-an-numpy-array
        p1_h = np.zeros((self.p1.shape[0], self.p1.shape[1]+1),self.p1.dtype)
        p1_h[:,:-1]= self.p1
        p1_h[:,2] = 1.0
        # https://stackoverflow.com/questions/26289972/use-numpy-to-multiply-a-matrix-across-an-array-of-points
        dest = p1_h.dot(H.T)
        # convert back to non-homogenous co-ordinates
        dest1 = np.zeros(self.p1.shape,self.p1.dtype)
        dest1[:,0] = dest[:,0] / dest[:,2]
        dest1[:,1] = dest[:,1] / dest[:,2]
        
        # Compute residuals, inmask and score 
        #   remember x and y is flipped in the image array
        #   point is inlier if both X and Y are within the the threshold
        residuals_y = dest1[:,0] - self.p2[:,0]
        residuals_x = dest1[:,1] - self.p2[:,1]
        score       = np.sum(residuals_x**2 + residuals_y**2)
        inmask_x    = np.abs(residuals_x) < t
        inmask_y    = np.abs(residuals_y) < t
        inmask      = np.logical_and(inmask_x,inmask_y)
        
        # count inliners, w
        n_in  = np.sum(inmask)
        n_out = len(self.p1) - n_in
        w = float(n_in)/(n_in+n_out)
        return n_in, n_out, w, score, inmask
    
    # Lets summarize the H matrix computed from all methods
    # 1) opencv homography for all points without RANSAC
    # 2) opencv homography for all points with opencv RANSAC
    # 3) opencv homography4 computed by our own RANSAC
    # 4) opencv homography for all inlier points determined by our own RANSAC
    def summarizeMethods(self,ransac1_H,ransac1_inmask,ransac2_H,ransac2_inmask,t=3):
        
        hSummary = {}
        H_cv_R_none,s1    = cv2.findHomography(self.p1, self.p2,0)
        H_cv_R_cv,s2      = cv2.findHomography(self.p1, self.p2,cv2.RANSAC,t)
        H_cv4_R_own       = ransac1_H
        H_own_R_own       = ransac2_H
        H_cvmask_R_own,s4 = cv2.findHomography(self.p1[ransac1_inmask], self.p2[ransac1_inmask],0)

        hSummary['H_cv_R_none']    = {}
        hSummary['H_cv_R_cv']      = {}
        hSummary['H_cv4_R_own']    = {}
        hSummary['H_own_R_own']    = {}
        hSummary['H_cvmask_R_own'] = {}
        
        hSummary['H_cv_R_none']['H']    = H_cv_R_none
        hSummary['H_cv_R_cv']['H']      = H_cv_R_cv
        hSummary['H_cv4_R_own']['H']    = H_cv4_R_own
        hSummary['H_own_R_own']['H']    = ransac2_H
        hSummary['H_cvmask_R_own']['H'] = H_cvmask_R_own
        
        for method in hSummary.keys():
            H = hSummary[method]['H']
            n_in, n_out, w, score, inmask = self.computeInliers(H,t)
            hSummary[method]['score'] = score
            hSummary[method]['n_in']  = n_in
            hSummary[method]['n_out'] = n_out
            hSummary[method]['w']     = w
        
        # print the details
        print ("\n\nSummary:\nResidual Threshold = %.2f" % t)
        print ("%-20s\t%-10s\t%-10s\t%-10s\t" % ("Method", '#inliners', 'Score', '#outliers'))
        for method in hSummary.keys():
            print ("%-20s\t%d\t\t%.2f\t%d" % 
                   (method,
                    hSummary[method]['n_in'],
                    hSummary[method]['score'],
                    hSummary[method]['n_out']))

    # Please refer to http://www.uio.no/studier/emner/matnat/its/UNIK4690/v16/forelesninger/lecture_4_3-estimating-homographies-from-feature-correspondences.pdf
    # for information about normalization for DLT.
    # We translate the centroid of the points to the origin and scale them such that the average distance of
    # the points from the origin is sqrt(2)
    @classmethod
    def NormalizePointsForDLT(cls, points):
      # We normalize the points using a matrix as below:
      # matrix = s x [1  0  -x]
      #              [0  0  -y]
      #              [0  0  -s]
      tx = np.average(points[:,0:1])
      ty = np.average(points[:,1:])
    
      scale = 0.0
      for i in range(0, len(points)):
        scale += np.sqrt((points[i, 0] - tx) * (points[i, 0] - tx) + (points[i, 1] - ty) * (points[i, 1] - ty))
    
      if scale == 0:
        scale = np.finfo(float).eps
    
      scale = np.sqrt(2.0) * len(points) / scale
    
      normalization_matrix = np.array([[scale, 0, -scale * tx], [0, scale, - scale * ty], [0, 0, 1]])
    
      homogeneous_points = np.append(points, np.ones([len(points), 1]), 1)
    
      for i in range(0, len(homogeneous_points)):
        homogeneous_points[i] = np.dot(normalization_matrix, np.transpose(homogeneous_points[i]))
    
      return (homogeneous_points, normalization_matrix)
    
    # Please refer to http://6.869.csail.mit.edu/fa12/lectures/lecture13ransac/lecture13ransac.pdf
    # http://vhosts.eecs.umich.edu/vision//teaching/EECS442_2011/lectures/discussion2.pdf
    # http://www.cs.cmu.edu/~16385/spring15/lectures/Lecture15.pdf for information about the matrix
    # A below. We try to solve an equation of the form A.H = 0 where H is the desired homography
    # matrix.
    @classmethod
    def GetHomographyMatrixUsingSVD(cls, points1, points2):
      points1_normalized, normalization_matrix1 = MatchPoints.NormalizePointsForDLT(points1)
      points2_normalized, normalization_matrix2 = MatchPoints.NormalizePointsForDLT(points2)
    
      A = []
      total_points = len(points1)
    
      for point_index in range(0, total_points):
        x1 = points1_normalized[point_index, 0]
        y1 = points1_normalized[point_index, 1]
        x2 = points2_normalized[point_index, 0]
        y2 = points2_normalized[point_index, 1]
    
        A.append([x1, y1, 1, 0, 0, 0, -x1*x2, -x2*y1, -x2])
        A.append([0, 0, 0, x1, y1, 1, -x1 * y2, -y1 * y2, -y2])
    
      U, D, V = np.linalg.svd(np.asarray(A), True)
      smallest_value_index = np.argmin(D)
    
      L = V[-1,:] / V[-1, -1]
    
      homography = L.reshape(3, 3)
      homography = np.dot(np.dot(np.linalg.inv(normalization_matrix2), homography), normalization_matrix1)
      return (homography, D[smallest_value_index])
            