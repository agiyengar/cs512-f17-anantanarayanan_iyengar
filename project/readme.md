Panorama image stitcher
============================================
Generates a Panoramic image from a set of images in a folder.

## Description
This project is based on [Matthew Brown's paper on Image Stitching using Invariant Features](http://matthewalunbrown.com/papers/ijcv2007.pdf).

We use OpenCV and numpy libraries in python for the most part.

Algorithms implemented by us are as below

- RANSAC for homography
  We implemented the RANSAC algorithm (Random Sample Consensus) algorithm for computing the homography matrix which allows us to transform images in one plane to another. The parameters to RANSAC like the inlier/outlier probability, number of samples, etc can be configured via the panorama.ini file. Please refer to the Usage Instructions section for details.

- Homography computation using SVD
  we implemented the algorithm to compute the homography matrix from the transformation matrix, which is created from matching keypoint pairs in a pair of images. This also required points to be normalized such that the average distance of points from the origin is sqrt(2). This has shown to give better results.

- Feather blending
  To ensure that the merged images blend well, we implemented feathered blending which gives values to a pixel in the overlap region between the images based on the closest image center.

- Connected Components.
  We build an image graph from the list of images in the image folder which is passed in via the command line. The edges in the graph are based on whether two images match with each other. Once the graph is built, we retrieve an ordered list of images based on connected component traversal, which ensures that we can successfully create panoramic images even if the list is unordered.

## Installation
The software is implemented in python and can be executed by copying the files into a folder. It has been tested with the Anaconda Python distribution on Mac OS Sierra 10.12.6 (16G29) and Windows 10. The software requires OpenCV and numpy libraries to be installed in python. For details on versions please refer below:
Python Version: 2.7.13 (Anaconda)
OpenCV version 3.1.0

## Usage Instructions
The software should be run with the following command.

python stitch.py <path to image directory>

For e.g. python stitch.py ../data/list1

If all goes well this should bring up a set of windows which show the image blending followed by an Panoramic image.  Please hit the ESC key on the Panoramic image Window to exit the software. The software supports a help key which can be displayed by hitting the 'h' key. The help is displayed in the console window.

There are a number of parameters available for configuration in the panorama.ini file, which needs to be in the same folder as the rest of the sources. Please refer to the panorama.ini file for details.

## Improvements
- Support Panoramic image straightening.
  If images are taken from a tilted camera, the resulting panoramic image would be distorted. This can be addressed by supporting cylindrical projection and correcting for the camera tilt.

- Bundle adjustment.
  The pairwise homography computed, may accumulate errors and affect the quality of the final panorama. Matt Brown proposed a technique known as Bundle adjustment that simultaneously optimizes all the parameters of the homography matrices together. This is done through a non-linear optimization method. We have not implemented this in our project and rely on pairwise homography.

