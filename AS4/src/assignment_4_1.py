import cv2
import numpy as np
import sys

def inch_to_mm(inch):
  return 25.4 * inch

# Programming Question 1 of Assignment 4. Provides functionality to detect chessboard
# corners and save them to a file. We use the opencv findChessboardCorners() and
# drawChessboardCorners() API's for this.
class Assignment4_1:
  def __init__(self):
    self.MAIN_WINDOW_TITLE = "Assignment 4 Question 1"
    cv2.startWindowThread()
    cv2.namedWindow(self.MAIN_WINDOW_TITLE)

    self.corners_refined = []
    # Parse the command line to display the image file or capture the video stream.
    self.ProcessCommandLine()
    self.OnNewImageLoaded()

  def ProcessCommandLine(self):
    if len(sys.argv) < 3:
      print("\nUsage. python assignment_4.1.py <image file name> <output file name>\n")
      self.Exit()

    self.original_image = cv2.imread(sys.argv[1])
    self.output_file_name = sys.argv[2]

  def OnNewImageLoaded(self):
    self.greyscale_image = cv2.cvtColor(self.original_image, cv2.COLOR_BGR2GRAY)
    # Threshold to black and white to ensure that the findChessboardCorners API works correctly.
    (thresh, im_bw) = cv2.threshold(self.greyscale_image, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

    cv2.imshow(self.MAIN_WINDOW_TITLE, self.greyscale_image)

    status, corners = cv2.findChessboardCorners(im_bw, (7, 7),
      cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_NORMALIZE_IMAGE
        + cv2.CALIB_CB_FAST_CHECK)
    if (status == False):
      print("Failed to detect corners in the image.")
      self.Exit()

    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.1)

    self.corners_refined = cv2.cornerSubPix(im_bw, corners, (11, 11), (-1, -1), criteria)
    self.image_with_corners = cv2.drawChessboardCorners(im_bw, (7, 7), self.corners_refined, status)
    cv2.imshow("Image with Corners", self.image_with_corners)

  def Exit(self):
    cv2.destroyAllWindows()
    sys.exit()

  def Help(self):
    print("\nThis is Programming question 1 of Assignment 4")
    print("\nUsage assignment_4_1.py <image_file> <output file name>\n")

    print("This program finds chessboard corners on the image using the OpenCV API findChessboardCorners()")
    print("\nSupported commands are as below:\n")
    print("h -> Display Help")
    print("s -> Save the image points found to a file.")
    print("<ESC> -> Exit\n")

  def GenerateWorldPoints(self, x_increment, y_increment, count):
    world_points = np.empty(([count, 3]))

    x = x_increment
    y = y_increment

    chessboard_col_count = 0

    for i in range(0, 49):
      world_points[i, 0] = inch_to_mm(x)
      world_points[i, 1] = inch_to_mm(y)
      world_points[i, 2] = 0

      x = x + x_increment

      chessboard_col_count = chessboard_col_count + 1
      if (chessboard_col_count > 6):
        chessboard_col_count = 0
        x = x_increment
        y = y + y_increment

    return world_points

  def Save(self):
    if (len(self.corners_refined) == 0):
      print("\nNo corners detected. Exiting")
      sys.Exit()

    print("Number of corners found:", len(self.corners_refined))

    # Generate world points.
    # The image I used had the squares of width 0.925 x 0.925
    world_points = self.GenerateWorldPoints(0.925, 0.925, 49)

    file = open(self.output_file_name, 'w')
    file.write("%d\n" %(len(self.corners_refined)))

    index = 0
    for item in self.corners_refined:
      file.write("%f %f %f %f %f\n" % (world_points[index, 0],
          world_points[index, 1], world_points[index, 2], item[0, 0], item[0, 1]))
      index = index + 1

    return True

  def ProcessCommand(self, key):
    if key == 27:
      self.Exit()
    elif key == ord('h'):
      self.Help()
    elif key == ord('s'):
      self.Save()

  def RunLoop(self):
    print("\n Please enter <ESC> to quit. <h> for Help\n")

    # For static images we wait indefinitely for the next key. For video frames
    # we process frames every 50 ms.
    timeout = 0

    while (True):
      key = cv2.waitKey(timeout)
      self.ProcessCommand(key)

instance = Assignment4_1()
instance.RunLoop()