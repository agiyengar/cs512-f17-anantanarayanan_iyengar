import cv2
import numpy as np
import sys
import math
import ConfigParser

# This class is required for RANSAC. It provides functionality to return a random
# sample from the sets of points and compute the score of the homography matrix.
class RansacMatcher:
  def __init__(self, points_1, points_2):
    # check if p1 and p2 is same length
    if len(points_1) != len(points_2):
        print ("ERROR: points_1 and points_2 not of same length")
        return
    self.points_1 = points_1
    self.points_2 = points_2
    self.N  = len(self.points_1)

  def GetSamples(self, number_of_samples = 40):
    # get 4 random indices
    r = np.random.randint(0, self.N, number_of_samples)

    random_points_1 = np.zeros((number_of_samples, 2), self.points_1.dtype)
    random_points_2 = np.zeros((number_of_samples, 2), self.points_2.dtype)

    for i in range(0, number_of_samples):
      random_points_1[i, :] = self.points_1[r[i], :]
      random_points_2[i, :] = self.points_2[r[i], :]
       
    return random_points_1, random_points_2

  def Score(self, homography, t=3):
    # Convert points_1 to homogeneous for transformation with homography.
    homogeneous_points_1 = np.append(self.points_1, np.ones([len(self.points_1), 1]), 1)

    destination = homogeneous_points_1.dot(homography.T)

    destination_non_homogenized = np.zeros(self.points_1.shape,self.points_1.dtype)

    # convert back to non-homogenous co-ordinates
    destination_non_homogenized = np.zeros(self.points_1.shape,self.points_1.dtype)
    destination_non_homogenized[:,0] = destination[:,0] / destination[:,2]
    destination_non_homogenized[:,1] = destination[:,1] / destination[:,2]
        
    # Compute residuals, inmask and score 
    # remember x and y is flipped in the image array
    # point is inlier if both X and Y are within the the threshold

    residuals_x = destination_non_homogenized[:,0] - self.points_2[:,0]
    residuals_y = destination_non_homogenized[:,1] - self.points_2[:,1]

    score     = np.sum(residuals_x**2 + residuals_y**2)
    inmask_x  = np.abs(residuals_x) < t
    inmask_y  = np.abs(residuals_y) < t
    inmask    = np.logical_and(inmask_x,inmask_y)
        
    # count inliers, w
    inliers  = np.sum(inmask)
    outliers = len(self.points_1) - inliers

    w = float(inliers) / (inliers + outliers)
    return inliers, outliers, w, score, inmask

# Provides functionality to compute the homography matrix using the sample of points
# returned by the RansacMatcher. The homography matrix is computed and scored based
# on the RANSAC algorithm. The class queries the inliers, outliers from the RansacMatcher
# during every iteration and then returns the best match.
class RansacHomographyEstimator:
  def __init__(self, matcher, base_instance, max_trials = 100, stop_probability = 0.9999999999,
               inliers_probability = 0.5, residual_threshold = 1):
    self.config_parser = ConfigParser.RawConfigParser()
    if (len(self.config_parser.read("RANSAC.config"))):
      print("\nFound RANSAC.config file. Reading parameters")

      max_trials = self.ReadIntegerFromConfig("Max Trials", max_trials)
      stop_probability = self.ReadFloatFromConfig("Outliers Probability", stop_probability)
      inliers_probability = self.ReadFloatFromConfig("Inliers Probability", inliers_probability)
      residual_threshold = self.ReadIntegerFromConfig("Threshold", residual_threshold)
      samples = self.ReadIntegerFromConfig("Number of Samples", 4)

    self.matcher = matcher
    self.max_trials = max_trials
    self.stop_probability = stop_probability
    self.inliers_probability = inliers_probability
    self.residual_threshold = residual_threshold
    self.k = 0

    if (inliers_probability != 1):
      self.k = np.abs(np.log(1- self.stop_probability) / np.log(1 - inliers_probability**2))

    self.base_instance = base_instance
    self.best = {}
    self.best['iter']  = 0
    self.best['inliers']  = 0
    self.best['outliers'] = 0
    self.best['homography'] = None
    self.best['score'] = 0
        
    self.best_inmask = 0 # this is different as we do not want to clutter
    self.book = {}

  def ReadIntegerFromConfig(self, value_name, default):
    int_value = default

    try:
      value = self.config_parser.get('Parameters', value_name)
      if (len(value) > 0):
        int_value = int(value)

    except ValueError:
      print("Exception while reading RANSAC.config. Missing value: \n", value_name)

    return int_value

  def ReadFloatFromConfig(self, value_name, default):
    float_value = default

    try:
      value = self.config_parser.get('Parameters', value_name)
      if (len(value) > 0):
        float_value = float(value)
    except:
      print("Exception while reading RANSAC.config. Missing value: \n", value_name)

    return float_value

  def ComputeHomography(self):
    i = 0
    while (i < self.max_trials):
      if (i >= self.k):
        print("Exceeded number of iterations\n")
        break

      random_points_1, random_points_2 = self.matcher.GetSamples()

      H, status = self.base_instance.GetHomographyMatrixUsingSVD(random_points_1, random_points_2)

      if H is None:
        continue

      inliers, outliers, w, score, inliers_mask = self.matcher.Score(H, self.residual_threshold)

      # if w is already 1, bail here.
      if (np.abs(w - 1) < 0.00001):
        print("Stopping here\n")
        self.k = i # we can stop here
      else:
        self.k = np.abs(np.log(1- self.stop_probability) / np.log(1 - w**2)) # sometimes we get -inf
            
      # record details
      self.book[i] = {}
      self.book[i]['inliers']  = inliers
      self.book[i]['outliers'] = outliers
      self.book[i]['homography'] = H
      self.book[i]['w'] = w
      self.book[i]['k'] = self.k
      self.book[i]['score'] = score
        
      if (self.best['inliers'] < inliers) or ( self.best['inliers'] == inliers and self.best['score'] < score):
        self.best['inliers']  = inliers
        self.best['outliers'] = outliers
        self.best['homography'] = H
        self.best['iter']  = i
        self.best['score'] = score
        self.best_inmask = inliers_mask

      i = i + 1
      # Avoid overflow here.
      if (i > self.max_trials < i):
        break

    self.DumpStatistics()
    return self.best['homography'], 1
        
  def DumpStatistics(self):
    for key in sorted(self.book.iterkeys()):
      print ("%d\t%.2f\t%d\t%d\t%.2f\t%.2f\t" % 
            (key,
            self.book[key]['score'],
            self.book[key]['inliers'],
            self.book[key]['outliers'],
            self.book[key]['w'],
            self.book[key]['k']))
      print (self.best)

# Programming Question 2 of Assignment 4. Provides functionality to compute internal and
# external parameters of the camera using planar calibration.
class Assignment4_2:
  def __init__(self):
    self.world_points = []
    self.image_points_1 = []
    self.image_points_2 = []
    self.image_points_3 = []
    self.translations = []
    self.rotations = []
    self.intrinsics = []
    self.MSE_0 = 0.0
    self.MSE_1 = 0.0
    self.MSE_2 = 0.0

    self.ProcessCommandLine()
    self.ReadFiles()

    homography_1, singular_value_1 = self.GetHomographyMatrixUsingSVD(self.world_points, self.image_points_1)
    homography_2, singular_value_2 = self.GetHomographyMatrixUsingSVD(self.world_points, self.image_points_2)
    homography_3, singular_value_3 = self.GetHomographyMatrixUsingSVD(self.world_points, self.image_points_3)

    try:
      self.Calibrate()
    except ValueError:
      print("Exception occured. Please try RANSAC by pressin r and ENTER")

  def ProcessCommandLine(self):
    if len(sys.argv) < 4:
      print("\nUsage. python assignment_4.1.py <image points file1, 2, 3>\n")
      self.Exit()

    self.image_points_file_name_1 = sys.argv[1]
    self.image_points_file_name_2 = sys.argv[2]
    self.image_points_file_name_3 = sys.argv[3]

  def ReadFile(self, file_name):
    line_count = 0
    image_points = []
    world_points = []
    with open(file_name, "r") as file:
      for input_line in file:
        if line_count > 0:
          point = input_line.split()
          world_points.append([float(point[0]), float(point[1])])
          image_points.append([float(point[3]), float(point[4])])
        line_count = line_count + 1
    return world_points, image_points

  def ReadFiles(self):
    try:
      self.world_points, self.image_points_1 = np.asarray(self.ReadFile(self.image_points_file_name_1))
      self.world_points, self.image_points_2 = np.asarray(self.ReadFile(self.image_points_file_name_2))
      self.world_points, self.image_points_3 = np.asarray(self.ReadFile(self.image_points_file_name_3))

      assert(len(self.world_points) == len(self.image_points_1) and
             len(self.image_points_1) == len(self.image_points_2) and
             len(self.image_points_2) == len(self.image_points_3))

    except IOError:
      print("\nFailed to open file. Exiting")
      self.Exit()

  # Please refer to http://www.uio.no/studier/emner/matnat/its/UNIK4690/v16/forelesninger/lecture_4_3-estimating-homographies-from-feature-correspondences.pdf
  # for information about normalization for DLT.
  # We translate the centroid of the points to the origin and scale them such that the average distance of
  # the points from the origin is sqrt(2)
  def NormalizePointsForDLT(self, points):
    # We normalize the points using a matrix as below:
    # matrix = s x [1  0  -x]
    #              [0  0  -y]
    #              [0  0  -s]
    tx = np.average(points[:,0:1])
    ty = np.average(points[:,1:])

    scale = 0.0
    for i in range(0, len(points)):
      scale += math.sqrt((points[i, 0] - tx) * (points[i, 0] - tx) + (points[i, 1] - ty) * (points[i, 1] - ty))

    if scale == 0:
      scale = np.finfo(float).eps

    scale = math.sqrt(2.0) * len(points) / scale

    normalization_matrix = np.array([[scale, 0, -scale * tx], [0, scale, - scale * ty], [0, 0, 1]])

    homogeneous_points = np.append(points, np.ones([len(points), 1]), 1)

    for i in range(0, len(homogeneous_points)):
      homogeneous_points[i] = np.dot(normalization_matrix, np.transpose(homogeneous_points[i]))

    return (homogeneous_points, normalization_matrix)

  # Please refer to http://6.869.csail.mit.edu/fa12/lectures/lecture13ransac/lecture13ransac.pdf
  # http://vhosts.eecs.umich.edu/vision//teaching/EECS442_2011/lectures/discussion2.pdf
  # http://www.cs.cmu.edu/~16385/spring15/lectures/Lecture15.pdf for information about the matrix
  # A below. We try to solve an equation of the form A.H = 0 where H is the desired homography
  # matrix.
  def GetHomographyMatrixUsingSVD(self, points1, points2):
    points1_normalized, normalization_matrix1 = self.NormalizePointsForDLT(points1)
    points2_normalized, normalization_matrix2 = self.NormalizePointsForDLT(points2)

    A = []
    total_points = len(points1)

    for point_index in range(0, total_points):
      x1 = points1_normalized[point_index, 0]
      y1 = points1_normalized[point_index, 1]
      x2 = points2_normalized[point_index, 0]
      y2 = points2_normalized[point_index, 1]

      A.append([x1, y1, 1, 0, 0, 0, -x1*x2, -x2*y1, -x2])
      A.append([0, 0, 0, x1, y1, 1, -x1 * y2, -y1 * y2, -y2])

    U, D, V = np.linalg.svd(np.asarray(A), True)
    smallest_value_index = np.argmin(D)

    L = V[-1,:] / V[-1, -1]

    homography = L.reshape(3, 3)
    homography = np.dot(np.dot(np.linalg.inv(normalization_matrix2), homography), normalization_matrix1)
    return (homography, D[smallest_value_index])

  def Calibrate(self):
    homography_1, singular_value_1 = self.GetHomographyMatrixUsingSVD(self.world_points, self.image_points_1)
    homography_2, singular_value_2 = self.GetHomographyMatrixUsingSVD(self.world_points, self.image_points_2)
    homography_3, singular_value_3 = self.GetHomographyMatrixUsingSVD(self.world_points, self.image_points_3)

    print("\nHomography 1: ")
    print(np.round(homography_1))

    print("\nHomography 2: ")
    print(np.round(homography_2))

    print("\nHomography 3: ")
    print(np.round(homography_3))

    homographies = []
    homographies.append(homography_1)
    homographies.append(homography_2)
    homographies.append(homography_3)

    self.CalibrateWithHomography(homographies)

  def CalibrateWithHomography(self, homographies):
    # Please look here http://staff.fh-hagenberg.at/burger/publications/reports/2016Calibration/Burger-CameraCalibration-20160516.pdf
    # for information about how we compute the camera parameters.
    self.intrinsics = self.CalculateCameraIntrinsics(homographies)
    self.rotations = []
    self.translations = []
    self.rotations, self.translations = self.CalculateCameraExtrinsics(self.intrinsics, homographies)
    print("\nInternal parameters of the camera are:\n")
    print(self.intrinsics.astype(int))

    for i in range(0, len(self.rotations)):
      print("\n")
      print("Rotation matrix number: ", i + 1)
      print(self.rotations[i])
      print("\n")
      print("Translation matrix number: ", i + 1)
      print(self.translations[i])

    self.EvaluateParameters()

  def CalculateCameraIntrinsics(self, homographies):
    # dimensions of V are 2M x 6 where M is the number of homographies.
    V = np.empty((2 * len(homographies), 6))

    # Every homography matrix contributes two rows to the matrix V.
    for i in range(0, len(homographies)):
      homography = homographies[i]
      V[2 * i] = self.GetRowVectorForHomography(0, 1, homography)
      V[2 * i + 1] = self.GetRowVectorForHomography(0, 0, homography) - self.GetRowVectorForHomography(1, 1, homography)

    U, D, V_transpose = np.linalg.svd(V, True)

    B = V_transpose[-1,:] / V_transpose[-1, -1]

    w = B[0] * B[2] * B[5] - B[1] * B[1] * B[5] - B[0] * B[4] * B[4] + 2 * B[1] * B[3] * B[4] - B[2] * B[3] * B[3]
    d = B[0] * B[2] - B[1] * B[1]

    alpha = math.sqrt(w / (d * B[0]))
    beta = math.sqrt(w / d ** 2 * B[0])
    gamma = math.sqrt(w / (d ** d * B[0]) * B[1])
    u0 = (B[1] * B[4] - B[2] * B[3]) / d
    v0 = (B[1] * B[3] - B[0] * B[4]) / d 

    intrinsics = np.empty((3, 3))
    intrinsics[0, 0] = alpha
    intrinsics[0, 1] = gamma
    intrinsics[0, 2] = u0
    intrinsics[1, 0] = 0
    intrinsics[1, 1] = beta
    intrinsics[1, 2] = v0
    intrinsics[2, 0] = 0
    intrinsics[2, 1] = 0
    intrinsics[2, 2] = 1
    return intrinsics

  def CalculateCameraExtrinsics(self, intrinsics, homographies):
    rotations = []
    translations = []
    for i in range(0, len(homographies)):
      homography = homographies[i]
      rotation, translation = self.GetExtrinsicsForView(intrinsics, homography)
      rotations.append(rotation)
      translations.append(translation)
    return rotations, translations

  def GetExtrinsicsForView(self, intrinsics, homography):
    h0 = homography[:, 0]
    h1 = homography[:, 1]
    h2 = homography[:, 2]

    inverse_intrinsics = np.linalg.inv(intrinsics)
    K = 1 / np.linalg.norm(np.dot(inverse_intrinsics, h0))
    r0 = K * np.dot(inverse_intrinsics, h0)
    r1 = K * np.dot(inverse_intrinsics, h1)
    r2 = np.cross(r0, r1)
    t = K * np.dot(inverse_intrinsics, h2)
    R = np.column_stack((r0, r1, r2))
    return R, t

  def GetRowVectorForHomography(self, p, q, homography):
    row_vector = np.empty((6, 1))
    row_vector[0, 0] = homography[0, p] * homography[0, q]
    row_vector[1, 0] = homography[0, p] * homography[1, q] + homography[1, p] * homography[0, q]
    row_vector[2, 0] = homography[1, p] * homography[1, q]
    row_vector[3, 0] = homography[2, p] * homography[0, q] + homography[0, p] * homography[2, q]
    row_vector[4, 0] = homography[2, p] * homography[1, q] + homography[1, p] * homography[2, q]
    row_vector[5, 0] = homography[2, p] * homography[2, q]
    return np.transpose(row_vector)

  def EvaluateParameters(self,):
    self.MSE_0 = self.ComputeMeanSquaredError(self.intrinsics, self.rotations[0], self.translations[0], self.world_points, self.image_points_1)
    self.MSE_1 = self.ComputeMeanSquaredError(self.intrinsics, self.rotations[1], self.translations[1], self.world_points, self.image_points_2)
    self.MSE_2 = self.ComputeMeanSquaredError(self.intrinsics, self.rotations[2], self.translations[2], self.world_points, self.image_points_3)

  def ComputeMeanSquaredError(self, intrinsics, rotation, translation, world_points, image_points):
    world_points = np.append(world_points, np.zeros([len(world_points), 1]), 1)
    homogeneous_world_points = np.append(world_points, np.ones([len(world_points), 1]), 1)

    error = 0.0
    for i in range(0, len(self.world_points)):
      camera_point = np.dot(np.column_stack((rotation, translation)), homogeneous_world_points[i])
      homogeneous_image_point = np.dot(intrinsics, camera_point)
      point = (homogeneous_image_point[0] / homogeneous_image_point[2], homogeneous_image_point[1] / homogeneous_image_point[2])
      error = error + (point[0] - image_points[i, 0]) ** 2 + (point[1] - image_points[i, 1]) ** 2
    
    error = np.round(error / len(image_points))
    print("Mean squared error is: ", error)
    return error

  def Exit(self):
    cv2.destroyAllWindows()
    sys.exit()

  def Help(self):
    print("\nThis is Programming question 2 of Assignment 4")
    print("\nUsage assignment_4_2.py <world points file name> <image points file1, 2, 3>\n")

    print("This program finds the internal and external parameters of the camera using planar calibration")
    print("\nSupported commands are as below:\n")
    print("h -> Display Help")
    print("r -> Compute parameters using RANSAC")
    print("q -> Exit\n")

  def ProcessCommand(self, key):
    if key == 'Q':
      self.Exit()
    elif key == 'H':
      self.Help()
    elif key == 'R':
      self.CalibrateUsingRANSAC()

  def CalibrateUsingRANSAC(self, number_of_samples = 4):
    try:
      ransac_1 = RansacHomographyEstimator(RansacMatcher(self.world_points, self.image_points_1), self)
      homography_1, status = ransac_1.ComputeHomography()
      if (homography_1 is None):
        print("*** RANSAC Failed to compute homography for image 1\n")
        return

      ransac_2 = RansacHomographyEstimator(RansacMatcher(self.world_points, self.image_points_2), self)
      homography_2, status = ransac_2.ComputeHomography()
      if (homography_2 is None):
        print("*** RANSAC Failed to compute homography for image 2\n")
        return

      ransac_3 = RansacHomographyEstimator(RansacMatcher(self.world_points, self.image_points_3), self)
      homography_3, status = ransac_3.ComputeHomography()
      if (homography_3 is None):
        print("*** RANSAC Failed to compute homography for image 3\n")
        return

      print("\nHomography 1: ")
      print(np.round(homography_1))

      print("\nHomography 2: ")
      print(np.round(homography_2))

      print("\nHomography 3: ")
      print(np.round(homography_3))

      homographies =[]
      homographies.append(homography_1)
      homographies.append(homography_2)
      homographies.append(homography_3)

      self.CalibrateWithHomography(homographies)

    except ValueError:
      print("Exception. Retrying\n")
      self.CalibrateUsingRANSAC()

  def RunLoop(self):
    print("\n Please enter <q> to quit. <h> for Help\n")

    # For static images we wait indefinitely for the next key. For video frames
    # we process frames every 50 ms.
    timeout = 0

    while (True):
      key = raw_input('\nPlease enter your choice and press ENTER\n')
      self.ProcessCommand(key.upper())

instance = Assignment4_2()
instance.RunLoop()