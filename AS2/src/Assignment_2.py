import cv2
import sys
import numpy
from scipy import ndimage
import scipy
from scipy import signal
import math
from sklearn import preprocessing

# Provides functionality for Assignment 2. Holds shared state for functionality
# required by the questions. Examples being the image, the processed image,
# the sobelX and Y filters, the blur filters etc.
class Assignment2:
  def __init__(self):
    self.WINDOW_TITLE = "Assignment 2"
    cv2.startWindowThread()

    # This flag controls the slider used to blur the image. If this is false
    # the blur has no effect.
    self.enabled_opencv_blur = False

    cv2.namedWindow(self.WINDOW_TITLE, cv2.WINDOW_NORMAL)

    # Parse the command line to display the image file or capture the video stream.
    self.ProcessCommandLine()

    self.sobelX = numpy.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]], dtype = numpy.float)
    self.sobelY = numpy.array([[-1, -2, -1], [0, 0, 0], [1, 2, 1]], dtype = numpy.float)

    # This member is used to cycle through the RGB channels of the image.
    self.last_channel = 0

    self.enabled_rotate = False
    self.enabled_gradient_vectors = False;
    self.enabled_custom_blur = False

    self.OnNewImageLoaded()

  def OnNewImageLoaded(self):
    # Get the red/green/blue channel images from the image. This allows us to quickly cycle through them.
    self.red_image = numpy.zeros((self.original_image.shape[0], self.original_image.shape[1], self.original_image.shape[2]), 'uint8')
    self.red_image[:, :, 2] = self.original_image[:, :, 2]

    self.green_image = numpy.zeros((self.original_image.shape[0], self.original_image.shape[1], self.original_image.shape[2]), 'uint8')
    self.green_image[:, :, 1] = self.original_image[:, :, 1]

    self.blue_image = numpy.zeros((self.original_image.shape[0], self.original_image.shape[1], self.original_image.shape[2]), 'uint8')
    self.blue_image[:, :, 0] = self.original_image[:, :, 0]

    if self.read_from_camera == False:
      self.enabled_rotate = False
      self.enabled_gradient_vectors = False;
      self.enabled_custom_blur = False

    # We work on the processed image each time. The original image is restored 
    # on request.
    self.processed_image = self.original_image
    cv2.imshow(self.WINDOW_TITLE, self.original_image)

  def ProcessCommandLine(self):
    if len(sys.argv) < 2:
      self.capture = cv2.VideoCapture(0)
      valid_frame, self.original_image = self.capture.read()
      if (valid_frame == False):
        print("\n**** Failed to read from camera *** \n")
        sys.exit()
      self.read_from_camera = True
    else:
      self.original_image = cv2.imread(sys.argv[1])
      self.read_from_camera = False

  def Reload(self):
    print("\nReloading image")
    self.processed_image = self.original_image
    cv2.imshow(self.WINDOW_TITLE, self.original_image)

  def Save(self):
    print("\nSaving image")
    cv2.imwrite("./out.jpg", self.processed_image)

  def OpenCVGrayScale(self):
    print("\nGrayscale conversion using opencv cvtColor")
    self.processed_image = cv2.cvtColor(self.original_image, cv2.COLOR_BGR2GRAY)
    cv2.imshow(self.WINDOW_TITLE, self.processed_image)

  def CustomGrayScale(self):
    print("\nCustom Grayscale conversion")
    if len(self.processed_image.shape) == 2:
      return
    # OpenCV loads images as BGR. We need to convert to Greyscale taking this into account. The image array
    # needs to be uint8. The numpy astype() call below converts to uint8.
    self.processed_image = numpy.average(self.processed_image, weights=[0.114, 0.587, 0.299], axis=2).astype(numpy.uint8)
    cv2.imshow(self.WINDOW_TITLE, self.processed_image)

  def CycleChannel(self):
    print("\nCycling channel. Current channel:", self.last_channel)
    if self.last_channel == 0:
      print("Displaying red channel")
      cv2.imwrite("./red.jpg", self.red_image)
      cv2.imshow(self.WINDOW_TITLE, self.red_image)
    elif self.last_channel == 1:
      print("Displaying green channel")
      cv2.imwrite("./green.jpg", self.green_image)
      cv2.imshow(self.WINDOW_TITLE, self.green_image)
    elif self.last_channel == 2:
      print("Displaying blue channel")
      cv2.imwrite("./blue.jpg", self.blue_image)
      cv2.imshow(self.WINDOW_TITLE, self.blue_image)

  def convolve_helper(self, image, kernel):
    (image_height, image_width) = image.shape[:2]
    output_image = numpy.zeros(self.processed_image.shape)

    (kernel_height, kernel_width) = kernel.shape[:2]
    pad = (kernel_width - 1) / 2

    # Replicate the pixels along the border of the image to ensure that the
    # width and height are not reduced.
    # https://www.pyimagesearch.com/2016/07/25/convolutions-with-opencv-and-python/
    image = cv2.copyMakeBorder(image, pad, pad, pad, pad, cv2.BORDER_REPLICATE)

    # Slide the kernel across the image
    for row in numpy.arange(pad, image_height + pad):
      for col in numpy.arange(pad, image_width + pad):
        # Numpy slicing to grab the input slice which we can convolve with the kernel.
        region_of_interest = image[row - pad : row + pad + 1, col - pad : col + pad + 1]
        # Element wise multiplication and then sum up.
        pixel_value = (region_of_interest * kernel).sum()
        output_image[row - pad, col - pad] = pixel_value
    return output_image.astype('uint8')

  def convolve_helper2(self, image, kernel):
    (image_height, image_width) = image.shape[:2]
    output_image = numpy.zeros(self.processed_image.shape)

    (kernel_height, kernel_width) = kernel.shape[:2]

    # Replicate the pixels along the border of the image to ensure that the
    # width and height are not reduced.
    # https://www.pyimagesearch.com/2016/07/25/convolutions-with-opencv-and-python/
    # Having no padding causes an ugly black strip near the border which increases in
    # size, as the kernel size increases.
    pad = (kernel_width - 1) / 2
    image = cv2.copyMakeBorder(image, pad, pad, pad, pad, cv2.BORDER_REPLICATE)

    for row in range(0, image_height):
      for col in range(0, image_width):
        pixel_value = 0
        x = row
        y = col

        for kernel_row in range(0, kernel_height):
          for kernel_col in range(0, kernel_width):
            pixel_value += kernel[kernel_row, kernel_col] * image[x, y]
            y += 1
          x += 1
          y = col

        output_image[row, col] = pixel_value
        pixel_value = 0
    return output_image.astype('uint8')

  def BlurCallback(self, slider_value):
    print("\nCallback. Slider value:", slider_value)
    if self.enabled_opencv_blur == False and self.enabled_custom_blur == False:
      return
    if slider_value > 0:
      # Minimum kernel size we support is 3.
      if (slider_value < 3):
        slider_value = 3
      # The kernel size should be odd.
      if (slider_value % 2 == 0):
        slider_value = slider_value + 1
      if self.enabled_opencv_blur == True:
        self.processed_image = cv2.blur(self.processed_image, (slider_value, slider_value))
        cv2.imshow(self.WINDOW_TITLE, self.processed_image)
      else:
        # We use a simple box filter for custom smoothing. This replaces the pixel
        # at the center of the filter by the simple average of its neighbors.
        custom_blur_matrix = numpy.ones((slider_value, slider_value), dtype = numpy.float)
        custom_blur_matrix = custom_blur_matrix / custom_blur_matrix.size

        self.processed_image = self.convolve_helper2(self.processed_image, custom_blur_matrix)
        cv2.imshow(self.WINDOW_TITLE, self.processed_image)

  def InitializeTrackbarHelper(self, trackbar_name, trackbar_limit, callback):
    trackbar_pos = cv2.getTrackbarPos(trackbar_name, self.WINDOW_TITLE)

    if self.read_from_camera == True:
      if (trackbar_pos > 0):
        cv2.setTrackbarPos(trackbar_name, self.WINDOW_TITLE, trackbar_pos)
        callback(trackbar_pos)
    else:
      cv2.setTrackbarPos(trackbar_name, self.WINDOW_TITLE, 0)

    if trackbar_pos == -1:
      cv2.createTrackbar(trackbar_name, self.WINDOW_TITLE, 0, trackbar_limit, callback)

  def BlurUsingOpenCV(self):
    print("\nBlur using opencv")
    self.enabled_opencv_blur = True
    self.OpenCVGrayScale()
    cv2.imshow(self.WINDOW_TITLE, self.processed_image)
    self.InitializeTrackbarHelper('Blur', 50, self.BlurCallback)

  def CustomBlur(self):
    print("\nCustom Blur")
    self.enabled_custom_blur = True
    self.CustomGrayScale()
    cv2.imshow(self.WINDOW_TITLE, self.processed_image)
    self.InitializeTrackbarHelper('Blur', 50, self.BlurCallback)

  def DownSample(self):
    print("\nDownSampling using opencv")
    self.processed_image = cv2.resize(self.processed_image, (self.processed_image.shape[0]/2, self.processed_image.shape[1]/2))
    cv2.imshow(self.WINDOW_TITLE, self.processed_image)

  def DownSampleWithSmoothing(self):
    print("\DownSampling with smoothing using opencv")
    self.processed_image = cv2.pyrDown(self.processed_image)
    cv2.imshow(self.WINDOW_TITLE, self.processed_image)

  def TakeXDerivative(self):
    print("\nX derivative")

    self.CustomGrayScale()

    self.processed_image = signal.convolve2d(
        self.processed_image, self.sobelX, mode='same', boundary = 'fill', fillvalue=0)

    # Normalize the image to the range [0, 255]
    self.processed_image *= 255.0 / self.processed_image.max()
    self.processed_image = self.processed_image.astype('uint8')
    cv2.imshow(self.WINDOW_TITLE, self.processed_image)

  def TakeYDerivative(self):
    print("\nY derivative")

    self.CustomGrayScale()

    self.processed_image = signal.convolve2d(
        self.processed_image, self.sobelY, mode='same', boundary = 'fill', fillvalue=0)

    # Normalize the image to the range [0, 255]
    self.processed_image *= 255.0/self.processed_image.max() 
    self.processed_image = self.processed_image.astype('uint8')
    cv2.imshow(self.WINDOW_TITLE, self.processed_image)

  def ShowGradientMagnitude(self):
    print("\nShow Gradient Magnitude")
    self.OpenCVGrayScale()

    x_image = signal.convolve2d(
        self.processed_image, self.sobelX, mode='same', boundary = 'fill', fillvalue=0)
    y_image = signal.convolve2d(
        self.processed_image, self.sobelY, mode='same', boundary = 'fill', fillvalue=0)

    magnitude = numpy.sqrt(x_image * x_image + y_image * y_image)
    # Normalize the magnitude to the range [0, 255]
    magnitude *= 255.0 / magnitude.max()
    magnitude = magnitude.astype('uint8')
    self.processed_image= magnitude
    cv2.imshow(self.WINDOW_TITLE, self.processed_image)

  def GradientCallback(self, pixels):
    if self.enabled_gradient_vectors == False:
      cv2.imshow(self.WINDOW_TITLE, self.processed_image)
      return

    if pixels == 0:
      cv2.imshow(self.WINDOW_TITLE, self.processed_image)
      return

    self.OpenCVGrayScale()

    num_rows, num_cols = self.processed_image.shape[:2]

    gradient_x, gradient_y = numpy.gradient(self.processed_image)
    gradient_magnitude = numpy.sqrt(gradient_x * gradient_x + gradient_y * gradient_y)

    # TODO(ananta)
    # The enumeration below to draw the gradient vectors does not seem like the 
    # right approach as the loop executes in Python, which would slow it down
    # considerably. I could not find a function in opencv which would do the plotting
    # for us. matplotlib appears to have the quiver functionality which seems like
    # it would do the right thing. However I ran out of time for investigating the
    # alternate approach.
    row = 0
    col = 0
    while row < num_rows:
      col = 0
      while col < num_cols:
        y_grad = gradient_y[row][col]
        x_grad = gradient_x[row][col]

        # If the gradient at x is 0, then it could mean a perpendicular
        # vector.
        if x_grad:
          angle = math.atan(y_grad / x_grad)
        else:
          angle = 1.5708 # angle value for 90 degrees.
        
        magnitude = gradient_magnitude[row][col] #math.sqrt(x_grad * x_grad + y_grad * y_grad)

        if magnitude:
          x = magnitude * math.cos(angle)
          y = magnitude * math.sin(angle)

          # The size of the gradient vector is assumed to be 10.
          x *= self.gradient_vector_length / magnitude
          y *= self.gradient_vector_length / magnitude

          x += row
          y += col

          cv2.arrowedLine(self.processed_image, (col, row), (int(y), int(x)), (0, 0, 255), 2)
        col += pixels
      row += pixels

    cv2.imshow(self.WINDOW_TITLE, self.processed_image)

  def PlotGradientVectors(self):
    print("\nPlot Gradient vectors")
    self.enabled_gradient_vectors = True
    self.OpenCVGrayScale()

    # This value holds the normalized gradient vector length. Please change this
    # if required.
    self.gradient_vector_length = 10

    # The trackbar limit is set to 200 pixels, i.e. 200 would be the max pixels we would
    # skip at any given point for the gradient.
    self.InitializeTrackbarHelper('Gradient', 200, self.GradientCallback)

  def RotateCallback(self, angle):
    if self.enabled_rotate == False:
      return

    num_rows, num_cols = self.processed_image.shape[:2]

    # This code comes from https://www.pyimagesearch.com/2017/01/02/rotate-images-correctly-with-opencv-and-python/
    # It handles the case when an image is cut off at the border.
    # We use the -ve of the angle to rotate clockwise. We then get the sine and cosine
    # i.e. the rotation components of the matrix.
    rotation_matrix = cv2.getRotationMatrix2D((num_cols // 2, num_rows // 2), -angle, 1)
    cos = numpy.abs(rotation_matrix[0, 0])
    sin = numpy.abs(rotation_matrix[0, 1])

    # Compute the new bounding dimensions. This ensures that no part of the image is cut off.
    new_width = int((num_rows * sin) + (num_cols * cos))
    new_height = int((num_rows * cos) + (num_cols * sin))

    # adjust the rotation matrix to take into account, the translation
    rotation_matrix[0, 2] += (new_width / 2) - num_cols // 2
    rotation_matrix[1, 2] += (new_height / 2) - num_rows // 2

    rotated_image = cv2.warpAffine(self.processed_image, rotation_matrix, (new_width, new_height))
    cv2.imwrite("./rotated.jpg", rotated_image)
    cv2.imshow(self.WINDOW_TITLE, rotated_image)

  def RotateImage(self):
    print("\nRotate image")

    self.enabled_rotate = True
    self.OpenCVGrayScale()
    # We support rotation by upto 360 degrees.
    self.InitializeTrackbarHelper('Rotate', 360, self.RotateCallback)

  def Help(self):
    print("\nThis is the programming portion of Assignment 2")
    print("\nAn image file can be passed via the command line. Assignment_2.py <image_file>")
    print("If an image file is not passed in, the program attempts to read frames from the camera\n")
    print("\nSupported commands are as below:\n")
    print("i -> Reload the image and discard current state")
    print("w -> Save the current image to the output file out.jpg")
    print("g -> Convert to grayscale via opencv")
    print("G -> Convert to grayscale without using opencv")
    print("c -> Cycle through the RGB channels of the image")
    print("s -> Convert to grayscale and smooth via opencv. A trackbar is provided to control smoothing")
    print("S -> Convert to grayscale and smooth via a convolution filter. A trackbar is provided to control smoothing")
    print("d -> Downsample the image by a factor of 2")
    print("D -> Downsample the image by a factor of 2 with smoothing")
    print("x -> Convert to grayscale and convolve with an x derivative filter. Normalize to [0, 255]")
    print("y -> Convert to grayscale and convolve with an y derivative filter. Normalize to [0, 255]")
    print("m -> Show the magnitude of the gradient normalized to [0, 255]")
    print("p -> Convert to grayscale and plot gradient vectors every N pixels. Use the trackbar to control N")
    print("r -> Convert to grayscale and rotate in an angle of theta. Use the trackbar to control theta ")
    print("h -> Display Help")
    print("<ESC> -> Exit")

  def Exit(self):
    if self.read_from_camera == True:
      self.capture.release()
    cv2.destroyAllWindows()
    sys.exit()

  def ProcessCommand(self, key):
    if key == 27:
      self.Exit()
    elif key == ord('i'):
      self.Reload()
    elif key == ord('w'):
      self.Save()
    elif key == ord('g'):
      self.OpenCVGrayScale()
    elif key == ord('G'):
      self.CustomGrayScale()
    elif key == ord('c'):
      self.CycleChannel()
    elif key == ord('s'):
      self.BlurUsingOpenCV()
    elif key == ord('S'):
      self.CustomBlur()
    elif key == ord('d'):
      self.DownSample()
    elif key == ord('D'):
      self.DownSampleWithSmoothing()
    elif key == ord('x'):
      self.TakeXDerivative()
    elif key == ord('y'):
      self.TakeYDerivative()
    elif key == ord('m'):
      self.ShowGradientMagnitude()
    elif key == ord('p'):
      self.PlotGradientVectors()
    elif key == ord('r'):
      self.RotateImage()
    elif key == ord('h'):
      self.Help()

  def RunLoop(self):
    print("\n Please enter <ESC> to quit. <h> for Help\n")

    # For static images we wait indefinitely for the next key. For video frames
    # we process frames every 50 ms.
    timeout = 0
    if self.read_from_camera == True:
      timeout = 50

    # Only applies for video frames. Used to execute the last command on the next
    # frame.
    last_key = -1

    while (True):
      key = cv2.waitKey(timeout)

      # Reset states like blur, rotate, gradient vector plots etc to ensure
      # that they don't mess up other commands.
      if self.enabled_opencv_blur == True or self.enabled_custom_blur == True:
        self.enabled_opencv_blur = False
        self.enabled_custom_blur = False

      if self.enabled_rotate == True:
        self.enabled_rotate = False

      if self.enabled_gradient_vectors == True:
        self.enabled_gradient_vectors = False

      self.ProcessCommand(key)

      # TODO(ananta)
      # Updating the internal state affected by a key should be handled in a better
      # way.
      if key == ord('c'):
        self.last_channel = (self.last_channel + 1) % 3

      if self.read_from_camera == True:
        valid_frame, self.original_image = self.capture.read()
        if (valid_frame == False):
          self.Exit()
        self.OnNewImageLoaded()
        if last_key != -1:
          self.ProcessCommand(last_key)
        if key != -1:
          last_key = key

instance = Assignment2()
instance.RunLoop()