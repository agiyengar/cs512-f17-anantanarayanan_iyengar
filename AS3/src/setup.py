from distutils.core import setup
from Cython.Build import cythonize
import numpy

setup(
  name = "Harris corner detection", 
  ext_modules = cythonize("src/harris_corner.pyx", build_dir="src"),
  include_dirs=[numpy.get_include()]
)