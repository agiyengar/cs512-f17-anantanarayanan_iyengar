import cv2
import numpy
import sys
cimport numpy as np

# Returns the key to be used for sorting.
cdef getKey(list item):
  return item[2]

# Performs non maximum suppression on the corners detected by the DetectCorners() function
# below.
# The |percent_of_points_to_return| parameter allows us to control the total number of
# corner points. Defaults to 100.
cdef NonMaximumSuppression(np.ndarray image, list list_of_corners, int suppression_radius,
                           float percent_of_points_to_return = 100.0):
  cdef np.ndarray color_image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)

  sorted(list_of_corners, key=getKey, reverse=True)

  cdef dict suppression_matrix = {}

  cdef int corner_number = 0

  cdef dict corner_map = {}

  cdef int points_to_return = round(percent_of_points_to_return / 100 * len(list_of_corners))
  print("Points to return: ", points_to_return, "Length: ", len(list_of_corners))
  
  for index, (y, x, lambda_xy, harris_value) in enumerate(list_of_corners):
    # If we have X% of points then return here.
    if (len(corner_map) >= points_to_return):
      break

    corner_map[corner_number] = [x, y, harris_value]

    if (suppression_matrix.get((x, y)) == None):
      for row in range(-suppression_radius, suppression_radius):
        for col in range(-suppression_radius, suppression_radius):
          suppress_col = x + col
          suppress_row = y + row
          suppression_matrix[(suppress_col, suppress_row)] = 1
      corner_number = corner_number + 1
      corner_map[corner_number] = [x, y, harris_value, corner_number]

      cv2.rectangle(color_image, (x - 10, y - 10), (x + 10, y + 10), (0, 0, 255), 1, 8, 0)

  return corner_map, color_image

# A corner point is the point to which the sum of projections of the gradients is minimum. This function
# attempts to find such a point.
# It solves a linear equation of the form V = C P where P is the corner point, C is the correlation matrix,
# V is the 2 x 1 vector which is computed for point in the neighborhood (edge).
cdef LocalizeCorners(list list_of_corners, int window_size, np.ndarray gradient_x, np.ndarray gradient_y, np.ndarray gradient_xy):
  localized_corners = []

  cdef np.ndarray edge_vector = numpy.zeros((2, 1))
  cdef np.ndarray gradient_matrix = numpy.zeros((2, 2))
  cdef np.ndarray point_matrix = numpy.zeros((2, 1))
  cdef np.ndarray temp_vector = numpy.zeros((2, 1))

  cdef int window_offset = window_size / 2

  cdef np.ndarray correlation_matrix = numpy.zeros((2, 2))

  for index, (y, x, lambda_xy, harris_value) in enumerate(list_of_corners):
    summation_x = 0
    summation_y = 0
    summation_xy = 0

    for row in range(y - window_offset, y + window_offset + 1):
      for col in range(x - window_offset, x + window_offset + 1):
        gradient_matrix[0, 0] = gradient_x[row, col]
        gradient_matrix[0, 1] = gradient_xy[row, col]
        gradient_matrix[1, 0] = gradient_xy[row, col]
        gradient_matrix[1, 1] = gradient_y[row, col]

        summation_x += gradient_x[row, col]
        summation_y += gradient_y[row, col]
        summation_xy += gradient_xy[row, col]

        point_matrix[0, 0] = row
        point_matrix[1, 0] = col
        
        temp_vector = numpy.dot(gradient_matrix, point_matrix)
        edge_vector = numpy.add(edge_vector, temp_vector)

    correlation_matrix[0, 0] = summation_x
    correlation_matrix[0, 1] = summation_xy
    correlation_matrix[1, 0] = summation_xy
    correlation_matrix[1, 1] = summation_y

    corner_point = numpy.dot(numpy.linalg.inv(correlation_matrix), edge_vector).astype('int32')

    edge_vector[0, 0] = 0
    edge_vector[1, 0] = 0

    localized_corners.append([corner_point[0, 0], corner_point[1, 0], lambda_xy, harris_value])

  return localized_corners

# Implementation of the Harris corner detection algorithm with additional functionality
# to localize the corner points accurately.
# image : The image to be operated on.
# window_title: The title of the window to display the corner results.
# window_size : The harris corner window size.
# gaussian : The gaussian blur sigma.
# trace_weight : K in the harris equation.
# threshold : Used to determine if there is a corner.
# suppression:  The pixels to be suppressed in non maximum suppression.
# percent_of_corner_points_to_return: Percentage of corner points to return. This affects
# non maximum suppression.

def DetectCorners(np.ndarray image, window_title, window_size, gaussian, trace_weight, threshold, suppression,
                  percent_of_corner_points_to_return):
  if (gaussian > 0):
    image = cv2.GaussianBlur(image, (0, 0), gaussian, gaussian, 0)

  cdef int image_height = image.shape[0]
  cdef int image_width = image.shape[1]

  print(image_height, image_width)

  # Thresholding the image improved the accuracy of the corner detection algorithm a lot. This is
  # to be expected as we don't detect spurious corners.
  # TODO(ananta)
  # Revisit this. The docs don't suggest that any form of thresholding is necessary. 
  cdef int threshold_val = 0
  cdef np.ndarray thresholded_image

  threshold_val, thresholded_image = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU);

  cdef np.ndarray gradient_x = cv2.Sobel(thresholded_image, cv2.CV_32F, 1, 0)
  cdef np.ndarray gradient_y = cv2.Sobel(thresholded_image, cv2.CV_32F, 0, 1)

  cdef np.ndarray Ixx = gradient_x * gradient_x
  cdef np.ndarray Iyy = gradient_y * gradient_y
  cdef np.ndarray Ixy = gradient_x * gradient_y

  cdef int window_offset = window_size / 2

  cdef np.ndarray correlation_matrix = numpy.zeros((2, 2))

  cdef list list_of_corners = []

  for i in range(window_offset, image_height - window_offset - 1):
    for j in range(window_offset, image_width - window_offset - 1):
      # The correlation matrix is as below:
      # Sum(Ixx)   Sum(Ixy)
      # Sum(Ixy)   Sum(Iyy)
      # Harris corner detection assumes a corner if the determinant of the above matrix
      # - K (trace of the matrix ^ 2) is above a threshold.
      # trace is computed as the sum of the diagonals of the above matrix..
      # K is the trace weight.

      # Get the window using numpy slicing
      window_x = Ixx[i - window_offset : i + window_offset + 1, j - window_offset : j + window_offset + 1]
      window_y = Iyy[i - window_offset : i + window_offset + 1, j - window_offset : j + window_offset + 1]
      window_xy = Ixy[i - window_offset : i + window_offset + 1, j - window_offset : j + window_offset + 1]

      summation_x = window_x.sum()
      summation_y = window_y.sum()
      summation_xy = window_xy.sum()

      determinant = (summation_x * summation_y) - (summation_xy * summation_xy)
      trace = summation_x + summation_y

      harris_value = determinant - trace_weight * (trace * trace)
      
      if harris_value > threshold:
        correlation_matrix[0, 0] = summation_x
        correlation_matrix[0, 1] = summation_xy
        correlation_matrix[1, 0] = summation_xy
        correlation_matrix[1, 1] = summation_y
        
        list_of_corners.append([i, j, summation_x * summation_y, harris_value])

  # Localize the detected corners by finding points to which the sum of projections to gradient vectors is minimum
  localized_corners = LocalizeCorners(list_of_corners, window_size, Ixx, Iyy, Ixy)

  # Suppress corners in the vicinity of the strong corners.
  corner_map, color_image = NonMaximumSuppression(image, localized_corners, suppression, percent_of_corner_points_to_return)
  cv2.imshow(window_title, color_image)
  return corner_map, color_image

# A corner point is the point to which the sum of projections of the gradients is minimum. This function
# attempts to find such a point.
# It solves a linear equation of the form V = C P where P is the corner point, C is the correlation matrix,
# V is the 2 x 1 vector which is computed for point in the neighborhood (edge).

# This function is not used anymore.
def OldLocalizeCorner(correlation_matrix, row_start, row_end, col_start, col_end, gradient_x, gradient_y, gradient_xy):
  edge_vector = numpy.zeros((2, 1))
  gradient_matrix = numpy.zeros((2, 2))
  point_matrix = numpy.zeros((2, 1))
  temp_vector = numpy.zeros((2, 1))

  row = 0
  for y in range(row_start, row_end):
    col = 0
    for x in range(col_start, col_end):
      gradient_matrix[0, 0] = gradient_x[row, col]
      gradient_matrix[0, 1] = gradient_xy[row, col]
      gradient_matrix[1, 0] = gradient_xy[row, col]
      gradient_matrix[1, 1] = gradient_y[row, col]

      point_matrix[0, 0] = y
      point_matrix[1, 0] = x

      temp_vector = numpy.dot(gradient_matrix, point_matrix)
      edge_vector = numpy.add(edge_vector, temp_vector)
      col = col + 1
    row = row + 1

  corner_point = numpy.dot(numpy.linalg.inv(correlation_matrix), edge_vector).astype('int32')
  return corner_point
