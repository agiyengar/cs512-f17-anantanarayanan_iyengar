import cv2
import numpy
import sys
import harris_corner as harris

# Provides functionality for Assignment 3. Holds shared state for functionality
# required by the question. This includes the image, the image being processed, etc.
class Assignment3:
  def __init__(self):
    self.MAIN_WINDOW_TITLE = "Assignment 3 Main Window"
    self.WINDOW2_TITLE = "Assignment 2 Second Window"
    cv2.startWindowThread()
    cv2.namedWindow(self.MAIN_WINDOW_TITLE, cv2.WINDOW_NORMAL)
    cv2.namedWindow(self.WINDOW2_TITLE, cv2.WINDOW_NORMAL)
    # Parse the command line to display the image file or capture the video stream.
    self.ProcessCommandLine()
    self.OnNewImageLoaded()

  def ProcessCommandLine(self):
    self.read_from_camera = False

    if len(sys.argv) < 2:
      print("Insufficient arguments. Please pass 1 or 0 after the file name to indicate if we are using opencv or not")
      self.Exit()

    self.use_open_cv = False
    self.use_open_cv = int(sys.argv[1])
    if (self.use_open_cv == True):
      print("Using OpenCV for corner detection\n")

    if (len(sys.argv) == 2):
      self.capture = cv2.VideoCapture(0)
      valid_frame, self.original_image = self.capture.read()
      if (valid_frame == False):
        print("\n**** Failed to read from camera *** \n")
        sys.exit()
      self.read_from_camera = True
    else:
      self.original_image = cv2.imread(sys.argv[2])
      self.read_from_camera = False
    
  def GaussianCallback(self, sigma):
    print("\nCallback. Sigma value:", sigma)
    self.DetectCorners()

  def WindowSizeCallback(self, window_size):
    print("\nCallback. Window Size:", window_size)
    self.DetectCorners()

  def TraceCallback(self, trace_weight):
    print("\nCallback. Trace Weight:", trace_weight)
    trace_weight = self.NormalizeTraceToRange(trace_weight, 0, 0.5)
    print(trace_weight)
    self.DetectCorners()

  def ThresholdCallback(self, threshold):
    print("\nCallback. threshold:", threshold)
    self.DetectCorners()

  def SuppressionRadiusCallback(self, suppression):
    print("\nCallback. suppression", suppression)
    self.DetectCorners()

  def NoiseCallback(self, sigma):
    print("\nCallback. Sigma value:", sigma)
    self.DetectCorners()

  def PercentCallback(self, percent):
    print("\nCallback. Percent value:", percent)
    self.DetectCorners()

  def OnNewImageLoaded(self):
    self.image_window_1 = cv2.cvtColor(self.original_image, cv2.COLOR_BGR2GRAY)
    self.image_window_2 = cv2.cvtColor(self.original_image, cv2.COLOR_BGR2GRAY)

    self.trackbar_init = True

    self.InitializeTrackbarHelper('Gaussian', 10, 0, self.GaussianCallback)
    self.InitializeTrackbarHelper('Window', 50, 5, self.WindowSizeCallback)
    self.InitializeTrackbarHelper('Trace', 50, 16, self.TraceCallback)
    self.InitializeTrackbarHelper('Threshold', 1000000, 10000, self.ThresholdCallback)
    self.InitializeTrackbarHelper("Suppression radius", 50, 25, self.SuppressionRadiusCallback)
    self.InitializeTrackbarHelper("Noise", 10, 0, self.NoiseCallback)
    self.InitializeTrackbarHelper("X percent", 100, 100, self.PercentCallback)

    self.trackbar_init = False

    cv2.imshow(self.MAIN_WINDOW_TITLE, self.image_window_1)
    cv2.imshow(self.WINDOW2_TITLE, self.image_window_2)

    self.DetectCorners()

  def NormalizeTraceToRange(self, trace, trace_min, trace_max):
    trace_weight = (trace_max - trace_min) * (trace) / 50
    return trace_weight

  def DetectCorners(self):
    if (self.trackbar_init == True):
      return
    gaussian = cv2.getTrackbarPos("Gaussian", self.MAIN_WINDOW_TITLE)
    window_size = cv2.getTrackbarPos("Window", self.MAIN_WINDOW_TITLE)
    trace = cv2.getTrackbarPos("Trace", self.MAIN_WINDOW_TITLE)
    threshold = cv2.getTrackbarPos("Threshold", self.MAIN_WINDOW_TITLE)
    trace_weight = self.NormalizeTraceToRange(trace, 0, 0.5)
    suppression = cv2.getTrackbarPos("Suppression radius", self.MAIN_WINDOW_TITLE)
    noise = cv2.getTrackbarPos("Noise", self.MAIN_WINDOW_TITLE)
    point_percent = cv2.getTrackbarPos("X percent", self.MAIN_WINDOW_TITLE)

    image_window_1  = self.AddNoise(self.image_window_1, noise).astype('uint8')
    image_window_2  = self.AddNoise(self.image_window_2, noise).astype('uint8')

    if self.use_open_cv == False:
      corner_info1, image1 = harris.DetectCorners(image_window_1, self.MAIN_WINDOW_TITLE, window_size, gaussian, trace_weight, threshold, suppression,
                                                  point_percent)
      corner_info2, image2 = harris.DetectCorners(image_window_2, self.WINDOW2_TITLE, window_size, gaussian, trace_weight, threshold, suppression,
                                                  point_percent)
      self.PrintMatchingCorners(corner_info1, corner_info2, image1, image2)
    else:
      self.DetectCornersUsingOpenCV(self.MAIN_WINDOW_TITLE, image_window_1, window_size, trace_weight, gaussian)
      self.DetectCornersUsingOpenCV(self.WINDOW2_TITLE, image_window_2, window_size, trace_weight, gaussian)

  def DetectCornersUsingOpenCV(self, window_title, original_image, window_size, trace_weight, gaussian):
    if (gaussian > 0):
      original_image = cv2.GaussianBlur(original_image, (0, 0), gaussian, gaussian, 0)

    image = cv2.cvtColor(original_image, cv2.COLOR_GRAY2RGB)
    corners = cv2.cornerHarris(original_image, window_size, 3, trace_weight)
    print("Corner list length = ", len(corners))
    #result is dilated for marking the corners, not important
    corners = cv2.dilate(corners, None)
    # Threshold for an optimal value, it may vary depending on the image.
    image[corners > 0.01 * corners.max()]= [0,0,255]
    cv2.imshow(window_title, image)

  def PrintMatchingCorners(self, corners_info1, corners_info2, image1, image2):
    image1_height = image1.shape[0]
    image1_width = image1.shape[1]

    for outer_key, outer_corner_info in corners_info1.iteritems():
      inner_corner_info = corners_info2.get(outer_key)
      if (inner_corner_info != None):
        cv2.putText(image1, str(outer_key), (outer_corner_info[0], outer_corner_info[1]),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.3, (0, 255, 0))
        cv2.putText(image2, str(outer_key), (inner_corner_info[0], inner_corner_info[1]),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.3, (0, 255, 0))

    cv2.imshow(self.MAIN_WINDOW_TITLE, image1)
    cv2.imshow(self.WINDOW2_TITLE, image2)

  def InitializeTrackbarHelper(self, trackbar_name, trackbar_limit, default, callback):
    trackbar_pos = cv2.getTrackbarPos(trackbar_name, self.MAIN_WINDOW_TITLE)

    if self.read_from_camera == True:
      if (trackbar_pos > 0):
        cv2.setTrackbarPos(trackbar_name, self.MAIN_WINDOW_TITLE, trackbar_pos)
        callback(trackbar_pos)

    if trackbar_pos == -1:
      cv2.createTrackbar(trackbar_name, self.MAIN_WINDOW_TITLE, 0, trackbar_limit, callback)
      cv2.setTrackbarPos(trackbar_name, self.MAIN_WINDOW_TITLE, default)

  def Exit(self):
    if self.read_from_camera == True:
      self.capture.release()
    cv2.destroyAllWindows()
    sys.exit()

  def Help(self):
    print("\nThis is the programming portion of Assignment 3")
    print("\nAn image file can be passed via the command line. Assignment_3.py <image_file>")
    print("If an image file is not passed in, the program attempts to read frames from the camera\n")
    print("\nSupported commands are as below:\n")
    print("h -> Display Help")
    print("<ESC> -> Exit\n")
    print("This program runs the Harris Corner detection algorithm on the same image loaded in two windows\n")
    print("It attempts to match corners detected in the main window with those in Window 2\n")
    print("The following parameters can be interactively controlled via trackbars in the main window.\n")
    print("1. Gaussian Blur scale\n")
    print("2. The neighborhood (window size) for corner detection\n")
    print("3. The weight of the trace parameter\n")
    print("4. The corner threshold\n")
    print("5. The suppression radius. This is used for non maxiumum suppression to suppress corners in the vicinity of a large corner\n")
    print("6. Gaussian noise to be mixed with the image\n")
    print("7. Percentage of corner ponts returned\n")

  def ProcessCommand(self, key):
    if key == 27:
      self.Exit()
    elif key == ord('h'):
      self.Help()

  def RunLoop(self):
    print("\n Please enter <ESC> to quit. <h> for Help\n")

    # For static images we wait indefinitely for the next key. For video frames
    # we process frames every 50 ms.
    timeout = 0
    if self.read_from_camera == True:
      timeout = 50

    while (True):
      key = cv2.waitKey(timeout)
      self.ProcessCommand(key)

      if self.read_from_camera == True:
        valid_frame, self.original_image = self.capture.read()
        if (valid_frame == False):
          self.Exit()
        self.OnNewImageLoaded()

  def AddNoise(self, image, sigma):
    if (sigma <= 0):
      return image
    row,col = image.shape
    mean = 0
    gauss = numpy.random.normal(mean, sigma, (row,col))
    gauss = gauss.reshape(row, col)
    noisy = image + gauss
    return noisy

instance = Assignment3()
instance.RunLoop()