# README #

Harris Corner Detector implementation.

Python implementation of the Harris Corner Detector. numpy and opencv are used for
functionality like reading an image, converting to grayscale, array and matrix manipulation, etc.

Usage is as below:

python src\Assignment_3.py <0|1> <optional image file name>

0 uses our Harris corner detection algorithm
1 uses opencv's implementation. This is useful for comparison.

If an image file is not passed in, we try reading images from the camera. 

The parameters for the Harris corner detection like the window size, suppression radius, gaussian blur,
the trace weight (k), percent of corner points, etc are controllable via track bars  in the main window

### Test data
The images used for testing have been uploaded to the data folder.

### Sources
The sources are in the src folder. 

### Executing the code.
The harris corner detection algorithm is implemented in a cython script. Please compile the script
prior to running the program.

python src/setup.py build_ext --build-lib src

