
# coding: utf-8

# Assignment 1, Question 5
# 
# Section A, B, C from Assignment 0
# 

# In[84]:

import numpy as np
import math
from sklearn import preprocessing

# Returns the angle in degrees between the vectors |v1| and |v2|
def angle(v1, v2):
    return math.degrees(math.acos(dotproduct(v1, v2) / (length(v1) * length(v2))))

# Returns the dotproduct of |v1| and |v2|
def dotproduct(v1, v2):
    return sum((a*b) for a, b in zip(v1, v2))

# Returns the magnitude of the vector |v|
def length(v):
    return math.sqrt(dotproduct(v, v))

# Returns a perpendicular vector to |v|.
# The cross product of two vectors is perpendicular to both
# vectors. Exception being parallel vectors. We take a cross
# product of the vector with [1 0 0] or [0 1 0], depending on
# whether the vector is parallel to [1 0 0]
def perpendicular(v):
    if v[1] == 0 and v[2] == 0:
        if v[0] == 0:
            raise ValueError('zero vector')
        else:
            return np.cross(v, [0, 1, 0])
    return np.cross(v, [1, 0, 0])

# Returns True if the rows in the matrix |m| form an orthogonal
# set, i.e. dot product of a row with every other row is 0.
def is_orthogonal_set(m):
    for row_index in range(m.shape[0]):
        for next_row_index in range(m.shape[0]):
            if (row_index == next_row_index):
                continue
            if (dotproduct(m[row_index], m[next_row_index]) != 0):
                return False
    
    return True                        

# This class solves Question A in assignment 0. Contains vectors and other
# datastructures specific to Question A.
class QuestionA:
    def __init__(self):
        self.A = np.array([1, 2, 3])
        self.B = np.array([4, 5, 6])
        self.C = np.array([-1, 1, 3])

    def A1(self):
        print("\nA1) 2A - B = \n", 2*self.A - self.B)
    
    def A2(self):
        x_axis = np.array([1, 0, 0])
        print("\nA2) ||A|| = ", length(self.A))
        print("Angle between A and X axis is ", angle(self.A, x_axis))
        
    def A3(self):
        print("\nA3) Unit vector in the direction of A is \n",
              preprocessing.normalize(self.A, norm='l2'))
        
    def A4(self):
        print("\nA4) Direction cosines of A are as below\n")
        for index, value in np.ndenumerate(self.A):
            print(value/length(self.A))

    def A5(self):
        print("\nA5) A.B = ", dotproduct(self.A, self.B))
        print("B.A = ", dotproduct(self.B, self.A))
        
    def A6(self):
        print("\nA6) Angle between A & B is ", angle(self.A, self.B))
    
    def A7(self):
        # There are infinite vectors perpendicular to a 3D vector. The
        # perpendicular function returns one such vector based on which
        # component of the input vector is zero. We use cross products for
        # the same.
        print("\nA7) Vector perpendicular to A is \n", perpendicular(self.A))
        # Another way is to assign values to all components of the perpendicular
        # vector except the last one and compute the last one programmatically.
        # We use the fact that the dot product of two vectors is 0 to compute the
        # value of the last component.
        sum = 0
        perpendicular_vector = np.ones(self.A.size - 1)
        sum = np.sum(self.A) - self.A[self.A.size - 1]
        last = -sum / self.A[self.A.size - 1]
        perpendicular_vector = np.append(perpendicular_vector, last)
        print("Another perpendicular vector is")
        print(perpendicular_vector)
        
    def A8(self):
        print("\nA8) A x B is \n", np.cross(self.A, self.B))
        print("B x A is \n", np.cross(self.B, self.A))
        
    def A9(self):
        print("\nA9) Vector perpendicular to A & B is \n", np.cross(self.A, self.B))
        
    def A10(self):
        print("\nA10)")
        # If the vectors can be expressed as a linear combination of the others then
        # they are linearly dependent. We use the least squares approximation to solve
        # an equation of the form Ax = b where A and x are two vectors and the output
        # is the third vector.
        A = np.column_stack([self.A, self.B])
        result = np.linalg.lstsq(A, self.C)
        if (np.nonzero(np.round(result[0]))):
            print("Linearly dependent because C can be expressed as linear combination of A & B")
            print(result[0])
            
        A = np.column_stack([self.A, self.C])
        result = np.linalg.lstsq(A, self.B)
        if (np.nonzero(np.round(result[0]))):
            print("Linearly dependent because B can be expressed as linear combination of A & C")
            print(result[0])
            
        A = np.column_stack([self.B, self.C])
        result = np.linalg.lstsq(A, self.A)
        if (np.nonzero(np.round(result[0]))):
            print("Linearly dependent because A can be expressed as linear combination of B & C")
            print(result[0])
            
    def A11(self):
        print("\nA11)")
        print("A transpose . B is")
        print(np.dot(np.transpose(self.A), self.B))
        
        print("A.B transpose is")
        print(np.dot(self.A, np.transpose(self.B)))
        
    def solve(self):
        self.A1()
        self.A2()
        self.A3()
        self.A4()
        self.A5()
        self.A6()
        self.A7()
        self.A8()
        self.A9()
        self.A10()
        self.A11()
        
# This class solves Question B in assignment 0. Contains vectors and other
# datastructures specific to Question B.
class QuestionB:
    def __init__(self):
        list = []
        list.append([1, 2, 3])
        list.append([4, -2, 3])
        list.append([0, 5, -1])
        self.A = np.asarray(list)
        list.clear()
        
        list.append([1, 2, 1])
        list.append([2, 1, -4])
        list.append([3, -2, 1])
        self.B = np.asarray(list)
        list.clear()
        
        list.append([1, 2, 3])
        list.append([4, 5, 6])
        list.append([-1, 1, 3])
        self.C = np.asarray(list)
        
    def B1(self):
        print("\nB1) 2A - B = \n", 2*self.A - self.B)
        
    def B2(self):
        print("\nB2) AB = \n", np.dot(self.A, self.B))
        print("BA = \n", np.dot(self.B, self.A))
        
    def B3(self):
        print("\nB3) AB transpose = \n", np.transpose(np.dot(self.A, self.B)))
        print("B transpose . A transpose = \n",
              np.dot(np.transpose(self.B), np.transpose(self.A)))
        
    def B4(self):
        print("\nB4) |A| is ", np.linalg.det(self.A))
        print("|C| is ", math.floor(np.linalg.det(self.C)))
        
    def B5(self):
        print("\nB5)")
        if (is_orthogonal_set(self.A)):
            print("\n Matrix A forms an orthogonal set")
        if (is_orthogonal_set(self.B)):
            print("\n Matrix B forms an orthogonal set")
        if (is_orthogonal_set(self.C)):
            print("\n Matrix C forms an orthogonal set")
            
    def B6(self):
        print("\nB6) A inverse is \n")
        print(np.linalg.inv(self.A))
        print("\nB inverse is \n")
        print(np.linalg.inv(self.B))

    def solve(self):
        self.B1()
        self.B2()
        self.B3()
        self.B4()
        self.B5()
        self.B6()
        
# This class solves Question C in assignment 0. Contains vectors and other
# datastructures specific to Question C.
class QuestionC:
    def __init__(self):
        list = []
        list.append([1, 2])
        list.append([3, 2])
        self.A = np.asarray(list)
        list.clear()
        list.append([2, -2])
        list.append([-2, 5])
        self.B = np.asarray(list)

        self.eigen_values_A, self.eigen_vectors_A = self.get_eigen_vectors(self.A)
        self.eigen_values_B, self.eigen_vectors_B = self.get_eigen_vectors(self.B)
        
    def get_eigen_vectors(self, matrix):
        eigen_values, eigen_vectors_matrix = np.linalg.eig(matrix)
        list = []
        # Each column contains an eigenvector
        for col_index in range(eigen_vectors_matrix.shape[1]):
            list.append(eigen_vectors_matrix[:,col_index])
        eigen_vectors = np.asarray(list)
        return eigen_values, eigen_vectors

    def C1(self):
        print("\nC1) The eigen values and eigen vectors of A are\n")
        print("eigenvalues:")
        print(self.eigen_values_A)
        print("eigenvectors:")
        print(self.eigen_vectors_A)
        
    def C2(self):
        list = []
        for row_index in range(self.eigen_vectors_A.shape[0]):
            list.append(self.eigen_vectors_A[row_index])
            
        V = np.asarray(list)
        V_inverse = np.linalg.inv(V)
        
        print("\nC2) V inverse.A.V is\n")
        print(np.round(np.dot(np.dot(V_inverse, self.A), V)))
        
    def C3(self):
        print("\nC3) The dot product between the eigenvectors of A is\n")
        print(np.dot(self.eigen_vectors_A[0], self.eigen_vectors_A[1]))
        
    def C4(self):
        print("\nC4) The dot product between the eigenvectors of B is\n")
        print(np.dot(self.eigen_vectors_B[0], self.eigen_vectors_B[1]))
        
    def C5(self):
        print("\nC5)")
        if (np.array_equal(np.transpose(self.B), self.B)):
            print("\nB is a symmetric matrix.")
        if (np.dot(self.eigen_vectors_B[0], self.eigen_vectors_B[1]) == 0):
            print("\nDot product of eigen vectors of B is 0, i.e they are orthogonal\n")

    def solve(self):
        self.C1()
        self.C2()
        self.C3()
        self.C4()
        self.C5()

question_A = QuestionA()
question_A.solve()

question_B = QuestionB()
question_B.solve()

question_C = QuestionC()
question_C.solve()