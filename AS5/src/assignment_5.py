import cv2
import numpy as np
import sys
from enum import Enum
import os

class ImageType(Enum):
  LEFT = 1
  RIGHT = 2

# This class provides functionality for solving Question 1 of Assignment 5, which is
# related with Epipolar Geometry. We load two images specified via command line
# parameters. The user can select 8 matching points between the images by clicking
# on corresponding points in the two images. Once we have 8 points entered by the
# user, we compute the fundamental matrix and display the epipole on the image if possible.
# Once the fundamental matrix is computed, the user can click on either image. We draw the
# epipolar line on the other image which corresponds to the point the user clicked on.

# The program also supports an additional command line argument after the file names,
# which is an integer. If this value is 1 we use SIFT for getting matching feature
# points between the two images. We compute the fundamental matrix with these points.
# This is useful for testing.
class Assignment5_1:
  def __init__(self):
    self.LEFT_WINDOW_TITLE = "Assignment 5. Epipolar lines Estimation. Left image"
    self.RIGHT_WINDOW_TITLE = "Assignment 5. Epipolar lines Estimation. Right image"

    cv2.startWindowThread()

    cv2.namedWindow(self.LEFT_WINDOW_TITLE)
    cv2.namedWindow(self.RIGHT_WINDOW_TITLE)

    self.compute_fundamental_matrix = True
    self.left_image_points = []
    self.right_image_points = []
    self.F = np.zeros((3, 3))

    self.ProcessCommandLine()

  def ProcessCommandLine(self):
    if len(sys.argv) < 3:
      print("\nUsage. python assignment_5.py <left_image file> <right_image file> <optional integer < 1 or 0 > to use SIFT for feature matching\n")
      self.Exit()

    self.left_image_file = sys.argv[1]
    self.right_image_file = sys.argv[2]

    self.left_image = cv2.imread(self.left_image_file, )
    self.right_image = cv2.imread(self.left_image_file, )

    self.left_image = cv2.resize(self.left_image, (600, 600))
    self.right_image = cv2.resize(self.right_image, (600, 600))
    self.use_sift = False

    if (len(sys.argv) == 4):
      self.use_sift = int(sys.argv[3])
      print("Use SIFT is ", self.use_sift)

    self.CreateGreyScaleImages()
    self.OnImagesReady()

    if (self.use_sift):
      self.GetSIFTMatches()
      self.ComputeFundamentalMatrix()

  def CreateGreyScaleImages(self):
    self.left_image_gray_scale = cv2.cvtColor(self.left_image, cv2.COLOR_BGR2GRAY)
    self.right_image_gray_scale = cv2.cvtColor(self.right_image, cv2.COLOR_BGR2GRAY)

    self.left_image_gray_scale = cv2.cvtColor(self.left_image_gray_scale, cv2.COLOR_GRAY2RGB)
    self.right_image_gray_scale = cv2.cvtColor(self.right_image_gray_scale, cv2.COLOR_GRAY2RGB)

  def OnImagesReady(self):
    cv2.imshow(self.LEFT_WINDOW_TITLE, self.left_image_gray_scale)
    cv2.imshow(self.RIGHT_WINDOW_TITLE, self.right_image_gray_scale)

  def LeftWindowMouseEventsCallback(self, event, x, y, flags, param):
    if (event == cv2.EVENT_LBUTTONDOWN):
      print("Got left window mouse click at ", x, y)
      self.HandleMouseClick(ImageType.LEFT, x, y, self.left_image_points)

  def RightWindowMouseEventsCallback(self, event, x, y, flags, param):
    if (event == cv2.EVENT_LBUTTONDOWN):
      print("Got right window mouse click at ", x, y)
      self.HandleMouseClick(ImageType.RIGHT, x, y, self.right_image_points)

  def HandleMouseClick(self, image_type, x, y, points_list):
    if (self.compute_fundamental_matrix == True):
      if (len(points_list) < 8):
        print("Appending point ", x, y, " to points list")
        points_list.append([x, y])
        if (image_type == ImageType.LEFT):
          cv2.circle(self.left_image_gray_scale, (x, y), 5, (255, 0, 0))
          cv2.imshow(self.LEFT_WINDOW_TITLE, self.left_image_gray_scale)
        else:
          cv2.circle(self.right_image_gray_scale, (x, y), 5, (255, 0, 0))
          cv2.imshow(self.RIGHT_WINDOW_TITLE, self.right_image_gray_scale)
  
      if (len(self.left_image_points) == 8 and len(self.right_image_points) == 8):
        print("Received 8 points in both images. We can now compute the fundamental matrix\n")
        self.ComputeFundamentalMatrix()
    else:
      self.DisplayEpipolarLine(image_type, x, y)

  # Normalizes the |points| array be subtracting the mean from the x & y columns
  # and dividing them by the standard deviation. We return the normalized homogeneous
  # points and the normalization matrix.
  def NormalizePoints(self, points):
    mean_x = np.average(points[:,0:1])
    mean_y = np.average(points[:,1:])

    sigma_x = np.std(points[:,0:1])
    sigma_y = np.std(points[:,1:])

    normalization_matrix = np.array([[1 / sigma_x, 0, -mean_x / sigma_x], [0, 1 / sigma_y, - mean_y / sigma_y], [0, 0, 1]])
    
    homogeneous_points = np.append(points, np.ones([len(points), 1]), 1)

    for i in range(0, len(homogeneous_points)):
      homogeneous_points[i] = np.dot(normalization_matrix, np.transpose(homogeneous_points[i]))

    return homogeneous_points, normalization_matrix

  # Computes the fundamental matrix which is used for computing epipolar points and lines.
  def ComputeFundamentalMatrix(self):
    left_points = np.asarray(self.left_image_points)
    right_points = np.asarray(self.right_image_points)

    left_points_normalized, normalization_matrix_left = self.NormalizePoints(left_points)
    right_points_normalized, normalization_matrix_right = self.NormalizePoints(right_points)

    A = []
    total_points = len(left_points)

    for point_index in range(0, total_points):
      xl = left_points_normalized[point_index, 0]
      yl = left_points_normalized[point_index, 1]

      xr = right_points_normalized[point_index, 0]
      yr = right_points_normalized[point_index, 1]

      A.append([xr * xl, xr * yl, xr, yr * xl, yr * yl, yr, xl, yl, 1])

    U, D, V = np.linalg.svd(np.asarray(A), True)

    F = V[-1].reshape(3, 3)

    # Compute the singular matrix F0 by decomposing the F matrix we got above
    # with SVD, zeroing the last diagonal element of D and then composing the
    # matrices back. This ensures that the fundamental matrix is rank 2.
    U, D, V = np.linalg.svd(F, True)

    D[2] = 0

    F0 = np.dot(U, np.dot(np.diag(D), V))
    F0 = np.dot(normalization_matrix_right.T, np.dot(F0, normalization_matrix_left))
    F0 = F0 / F0[2, 2]

    self.F = F0
    self.compute_fundamental_matrix = False

    print("Fundamental matrix is as below\n")
    print(self.F)

    self.ComputeReprojectionErrors()

    self.DisplayEpipoles()

    print("\nPlease click on either of the images to see the epipolar line in the other image.")

  def GetEpipole(self, F):
    U, D, V = np.linalg.svd(F, True)
    L = V[-1] / V[-1,-1]
    return (L[0] / L[2], L[1] / L[2])

  def DisplayEpipoles(self):
    if (self.compute_fundamental_matrix == True):
      print("Fundamental matrix not available yet. Cannot compute epipoles\n")
      return

    print("Computing epipoles\n")

    self.right_epipole = self.GetEpipole(self.F)
    self.left_epipole = self.GetEpipole(self.F.T)

    #self.left_epipole = np.round(self.left_epipole).astype(int)
    #self.right_epipole = np.round(self.right_epipole).astype(int)

    print("Left epipole is ", self.left_epipole)
    print("Right epipole is ", self.right_epipole)

    cv2.circle(self.left_image_gray_scale, (int(self.left_epipole[0]), int(self.left_epipole[1])), 5, (0, 0, 255))
    cv2.circle(self.right_image_gray_scale, (int(self.right_epipole[0]), int(self.right_epipole[1])), 5, (0, 0, 255))

    left_epipole_info = "Epipole " + str(self.left_epipole[0]) + "," + str(self.left_epipole[1])
    right_epipole_info = "Epipole " + str(self.right_epipole[0]) + "," + str(self.right_epipole[1])

    cv2.putText(self.left_image_gray_scale, left_epipole_info, (int(self.left_epipole[0]) + 6, int(self.left_epipole[1])),
            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))
    cv2.putText(self.right_image_gray_scale, right_epipole_info, (int(self.right_epipole[0]) + 6, int(self.right_epipole[1])),
            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))

    cv2.imshow(self.LEFT_WINDOW_TITLE, self.left_image_gray_scale)
    cv2.imshow(self.RIGHT_WINDOW_TITLE, self.right_image_gray_scale)

  def DisplayEpipolarLine(self, image_type, x, y):
    rows, cols = self.left_image_gray_scale.shape[:2]

    epipolar_line = np.empty((3, 1))

    if (image_type == ImageType.LEFT):
      epipolar_line = self.GetEpipolarLine(self.F, x, y)
    else:
      epipolar_line = self.GetEpipolarLine(self.F.T, x, y)
   
    x0, y0 = map(int, [0, -epipolar_line[2] / epipolar_line[1]])
    x1, y1 = map(int, [cols, -(epipolar_line[2] + epipolar_line[0] * cols) / epipolar_line[1]])

    print("The epipolar line will be drawn between points ", x0, y0, " and ", x1, y1)

    if (image_type == ImageType.LEFT):
      cv2.circle(self.left_image_gray_scale, (x, y), 5, (0, 255, 0))
      self.right_image_gray_scale = cv2.line(self.right_image_gray_scale, (x0,y0), (x1,y1), (0, 0, 255), 1)
    else:
      cv2.circle(self.right_image_gray_scale, (x, y), 5, (0, 255, 0))
      self.left_image_gray_scale = cv2.line(self.left_image_gray_scale, (x0,y0), (x1,y1), (0, 0, 255), 1)

    cv2.imshow(self.LEFT_WINDOW_TITLE, self.left_image_gray_scale)
    cv2.imshow(self.RIGHT_WINDOW_TITLE, self.right_image_gray_scale)

  def Exit(self):
    cv2.destroyAllWindows()
    sys.exit()

  def Help(self):
    print("\nThis is Programming question 1 of Assignment 5. Epipolar lines estimation")
    print("\nUsage. python assignment_5.py <left_image file> <right_image file> <optional integer < 1 or 0 > to use SIFT for feature matching\n")

    print("This program computes the fundamental matrix using the points specified by the user or by SIFT feature matching.")
    print("To specify points, please click on the left and right images in order.")
    print("The fundamental matrix will be calculated using these points and displayed in the console window.")
    print("You can then click anywhere in the left/right image. The epipolar line will be displayed in the other image.")

    print("\nSupported commands are as below:\n")
    
    print("h -> Display Help")
    print("ESC -> Exit")
    print("s -> Save left and right images. The output images are saved in the current folder with filenames starting with the out prefix")

  def Save(self):
    print("\nSaving images")
    head_left, tail_left = os.path.split(self.left_image_file)
    head_right, tail_right = os.path.split(self.right_image_file)

    output_left_file_name = "out_"
    output_right_file_name = "out_"

    if self.use_sift == True:
      output_left_file_name += "sift_"
      output_right_file_name += "sift_"

    output_left_file_name += tail_left
    output_right_file_name += tail_right

    cv2.imwrite(output_left_file_name, self.left_image_gray_scale)
    cv2.imwrite(output_right_file_name, self.right_image_gray_scale)

  def ProcessCommand(self, key):
    if key == 27:
      self.Exit()
    elif key == ord('h'):
      self.Help()
    elif key == ord('s'):
      self.Save()

  def GetSIFTMatches(self):
    sift = cv2.xfeatures2d.SIFT_create()

    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(self.left_image_gray_scale, None)
    kp2, des2 = sift.detectAndCompute(self.right_image_gray_scale, None)

    matcher = cv2.BFMatcher()
    matches = matcher.knnMatch(des1, des2, k=2)

    good = []
    pts1 = []
    pts2 = []

    # ratio test as per Lowe's paper
    for i,(m,n) in enumerate(matches):
      if m.distance < 0.8 * n.distance:
        good.append(m)
        pts2.append(kp2[m.trainIdx].pt)
        pts1.append(kp1[m.queryIdx].pt)

    self.left_image_points = np.int32(pts1).tolist()
    self.right_image_points = np.int32(pts2).tolist()

  def GetEpipolarLine(self, F, x, y):
    epipolar_line = np.empty((3, 1))
    homogeneous_point = np.array([x, y, 1])
    return np.dot(F, homogeneous_point)

  # Compute the reprojection error by computing the epipolar lines for the right
  # image for each point in the left image we used for computing the fundamental matrix.
  # To compute the MSE we compute the right epipolar lines and then substitute the value
  # of the corresponding point from the right image into the epipolar line coeffecients.
  # The expectation is that the points should satisfy the standard line equation
  # ax + by + c = 0. If they don't then we track the squared error and then average it
  # out for the mean squared error.
  def ComputeReprojectionErrors(self):
    assert(len(self.left_image_points) == len(self.right_image_points))
    
    mean_squared_error = 0

    for i in range(0, len(self.left_image_points)):
      line = self.GetEpipolarLine(self.F, self.right_image_points[i][0], self.right_image_points[i][1])
      result = line[0] * self.right_image_points[i][0] + line[1] * self.right_image_points[i][1] + line[2]
      mean_squared_error = mean_squared_error + result ** 2
    
    mean_squared_error = mean_squared_error / len(self.left_image_points)

    print("Reprojection mean squared error (MSE) is  ", mean_squared_error)

  def RunLoop(self):
    print("\n Please hit the <ESC> key to quit. <h> for help\n")

    cv2.setMouseCallback(self.LEFT_WINDOW_TITLE, self.LeftWindowMouseEventsCallback)
    cv2.setMouseCallback(self.RIGHT_WINDOW_TITLE, self.RightWindowMouseEventsCallback)

    while (True):
      key = cv2.waitKey(0)
      self.ProcessCommand(key)

instance = Assignment5_1()
instance.RunLoop()
