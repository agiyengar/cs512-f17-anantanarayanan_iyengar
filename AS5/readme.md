# README #

Assignment 5. Epipolar line estimation for stereo image pairs.

Python implementation for Epipolar line estimation for stereo image pairs. The numpy and
opencv libraries have been used for functionality like reading images, converting to grayscale,
drawing the epipolar lines, etc.

Usage is as below:

python src\assignment_5.py <stereo image left> <stereo image right> <optional parameter 1|0 to use SIFT for feature points>

### Test data
Images used for testing are in the data folder. Some of them are from the course website. Others from general google searches.

## Sources
The source file assignment.py is in the src folder.

## Documents
The report for the programming assignment and the answers to the review questions are in the doc folder.